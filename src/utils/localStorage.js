import { LOGIN_STORAGE, DATA_LOCAL } from "src/config/Constant";

export const getStorage = (name) => {
    return localStorage.getItem(name);
}

export const setStorage = (name, data) => {
    localStorage.setItem(name, JSON.stringify(data));
}

export const isLogin = () => {
    let login = localStorage.getItem(LOGIN_STORAGE)
    if (login) {
        return JSON.parse(login);
    } else {
        return false;
    }
}

export const setLogin = (state) => {
    localStorage.setItem(LOGIN_STORAGE, JSON.stringify(state));
}

export const addLocalStorage = (state) => {
    let data = JSON.parse(localStorage.getItem(DATA_LOCAL));
    if (data) {
        let json = data;
        if (state.cart) {
            let carts = [];
            if (json.carts) {
                carts = json.carts;
                carts.push(state.cart);
                localStorage.setItem(DATA_LOCAL, JSON.stringify({ ...json, carts: carts }));
            } else {
                carts.push(state.cart);
                localStorage.setItem(DATA_LOCAL, JSON.stringify({ ...json, carts: carts }));
            }
        } else {
            localStorage.setItem(DATA_LOCAL, JSON.stringify({ ...json, ...state }));
        }
    } else {
        localStorage.setItem(DATA_LOCAL, JSON.stringify(state));
    }
}

export const getLocalStorage = () => {
    let data = JSON.parse(localStorage.getItem(DATA_LOCAL));
    if (data) {
        return data;
    } else {
        return {};
    }
}

export const removeLocalStorage = () => {
    localStorage.removeItem(DATA_LOCAL);
}
