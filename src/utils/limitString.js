const limitString = (str, wordNumber) => {
    let strLength = str.length;
    let final = ""
    if (strLength > wordNumber) {
        final = str.slice(0, wordNumber) + " ...";
    }else{
        final = str;
    }
    return final;
}

export default limitString;