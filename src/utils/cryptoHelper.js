import CryptoJS from "crypto-js";

export const DecryptScript = (encrypt) => {
    if (encrypt !== "") {
        var bytes = CryptoJS.AES.decrypt(encrypt, 'MKPMOBILELIVESTYLE12345678');
        var originalText = bytes.toString(CryptoJS.enc.Utf8);
        return originalText;
    } else {
        return "";
    }
}