const initialState = {
  isLoading: false,
  tcsProduct: {},
  initialState: {},
  schema: {},
  cart: []
};
const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_TCS_PRODUCT':
      return {
        ...state,
        isLoading: false,
        tcsProduct: action.payload,
      };
    case 'ADD_INITIAL_STATE_FORM':
      return {
        ...state,
        isLoading: false,
        initialState: action.payload,
      };
    case 'ADD_SCHEMA_FORM':
      return {
        ...state,
        isLoading: false,
        schema: action.payload,
      };
    default:
      return state;
  }
};
export default rootReducer;
