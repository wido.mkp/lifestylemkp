export const addTcsProduct = props => {
  return props.dispatch(
    {
      type: 'ADD_TCS_PRODUCT',
      payload: props.body,
    }
  );
};

export const addInitialStateForm = props => {
  return props.dispatch(
    {
      type: 'ADD_INITIAL_STATE_FORM',
      payload: props.body,
    }
  );
};

export const addSchemaForm = props => {
  return props.dispatch(
    {
      type: 'ADD_SCHEMA_FORM',
      payload: props.body,
    }
  );
};
