import colors from "src/config/Color";

const useStyles = theme => ({
    container: {
        margin: 'auto',
        width: '360px',
        height: '100vh',
        textAlign: 'center',
        position: 'fixed',
        marginLeft: "auto",
        marginRight: "auto",
        left: 0,
        right: 0,
        textAlign: "center",
        backgroundColor: colors.bgColor,
        [theme.breakpoints.down('sm')]: {
            width: '100%',
        },
        paddingLeft: "25px",
        paddingRight: "25px",
        fontFamily: "Poppins",
        overflowY:"scroll"
    },
    layoutWrapper: {
        paddingBottom: "60px",
        scrollbarWidth: "none",

    },
    layoutHeaderWrapper: {
        paddingLeft: '40px',
        paddingRight: '40px',
    },
    bgWrapper: {
        background: `url("/images/bg.png")`,
        backgroundRepeat: 'no-repeat',
        width: '100%',
        height: '30%',
        right: 0,
        left: 0,
        display: 'flex',
        flexDirection: 'column',
        top: '0',
        position: 'absolute',
        objectFit: 'contain',
        backgroundSize: '100% 60%',
    },
    bottomContainer: {
        position: "fixed",
        bottom: 0,
        right: 0,
        left: 0,
        [theme.breakpoints.down('sm')]: {
            width: '100%',
        },
        width: "360px",
        marginLeft: "auto",
        marginRight: "auto",
        zIndex: 10000,
        height: "65px",
        backgroundColor: colors.btnColor,
        borderStyle: "solid",
        borderTopWidth: 0.03,
        borderBottomWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderColor: "#ADADAD"
    }
});
export default useStyles;