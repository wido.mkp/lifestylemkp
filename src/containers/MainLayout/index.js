import Grid from "@mui/material/Grid";
// import BottomNavigationModule from "./BottomNavigationModule";
import useStyles from "./MainLayout.module.js";
import { useTheme } from "@mui/styles";
import React from "react";
import LoadingScreen from "@/components/LoadingScreen/LoadingScreen";

const MainLayout = ({ disableNav, children, usePadding = true }) => {
    const theme = useTheme();
    const styles = useStyles(theme);
    // const [loading, setLoading] = React.useState(false);
    const [padding, setPadding] = React.useState("0rem");
    // React.useEffect(() => {
    //     setLoading(true);
    //     setTimeout(() => {
    //         setLoading(false);
    //     }, 500)
    // }, []);

    React.useEffect(() => {
        if (typeof window !== "undefined" && typeof navigator !== "undefined") {
            let userAgent = navigator.userAgent || navigator.vendor || window.opera;
            if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
                setPadding("5rem");
            } else {
                setPadding("0.1rem");
            }
        }
    }, []);

    // if (loading) {
    //     return (
    //         <LoadingScreen />
    //     )
    // }
    return (
        <>
            <Grid sx={usePadding ? { ...styles.container, paddingBottom: padding } : { ...styles.container, paddingLeft: 0, paddingRight: 0, paddingBottom: padding }}>
                <Grid sx={styles.layoutWrapper}>
                    {
                        children
                    }
                </Grid>
                {/* {
                    disableNav && disableNav === true ? null : <BottomNavigationModule />
                } */}
            </Grid>
        </>
    );
}

export default MainLayout