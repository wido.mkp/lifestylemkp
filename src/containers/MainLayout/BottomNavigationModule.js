import React from "react";
import { useRouter } from 'next/router'
import { useTheme } from '@mui/material/styles';
import useStyles from './MainLayout.module';
import { Grid } from "@mui/material";
import Image from "next/image";
import { toast } from 'react-toastify';
import colors from "src/config/Color";
const menu = [
    {
        id: 1,
        src: "/assets/images/home.png"
    },
    {
        id: 2,
        src: "/assets/images/ticket.png"
    },
    {
        id: 3,
        src: "/assets/images/notification.png"
    },
    {
        id: 4,
        src: "/assets/images/profile.png"
    },
]
const BottomNavigationModule = () => {
    const router = useRouter()
    const [value, setValue] = React.useState(-1);
    const theme = useTheme();
    const styles = useStyles(theme);
    React.useEffect(() => {
        getPathUrl();
    }, [])
    const getPathUrl = () => {
        let pathname = router.pathname;
        if (pathname.split("/")[1] === "") {
            setValue(0)
        } else if (pathname.split("/")[1] === "member") {
            setValue(1)
        } else if (pathname.split("/")[1] === "vehicle") {
            setValue(2)
        } else if (pathname.split("/")[1] === "profile") {
            setValue(3)
        }
    }
    return (
        <Grid sx={styles.bottomContainer} elevation={3}>
            <Grid
                sx={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "space-around",
                    alignItems: "center",
                    width: "100%",
                    height: "100%",
                }}
            >
                {
                    menu.map((item, index) => {
                        return (
                            <Grid
                                key={index}
                                sx={{
                                    width: "100px",
                                    height: "100%",
                                    cursor: "pointer",
                                    display: "flex",
                                    flexDirection: "row",
                                    justifyContent: "center",
                                    alignItems: "center",
                                    position: "relative"
                                }}
                                onClick={() => {
                                    setValue(index);
                                    if (index === 0) {
                                        router.push("/");
                                    } else if (index === 1) {
                                        toast.warning("Fitur belum tersedia!", {
                                            position: "bottom-center",
                                            autoClose: 5000,
                                            hideProgressBar: false,
                                            closeOnClick: true,
                                            pauseOnHover: true,
                                            draggable: true,
                                            progress: undefined,
                                        })
                                    } else if (index === 2) {
                                        toast.warning("Fitur belum tersedia!", {
                                            position: "bottom-center",
                                            autoClose: 5000,
                                            hideProgressBar: false,
                                            closeOnClick: true,
                                            pauseOnHover: true,
                                            draggable: true,
                                            progress: undefined,
                                        })
                                    } else if (index === 3) {
                                        toast.warning("Fitur belum tersedia!", {
                                            position: "bottom-center",
                                            autoClose: 5000,
                                            hideProgressBar: false,
                                            closeOnClick: true,
                                            pauseOnHover: true,
                                            draggable: true,
                                            progress: undefined,
                                        })
                                    }
                                }}
                            >
                                {/* {
                                    value === index ? <Grid key={index} sx={{
                                        width: "60px",
                                        height: "35px",
                                        borderRadius: "0 0 30px 30px",
                                        borderStyle: "solid",
                                        borderColor: colors.bgColor,
                                        borderWidth: "8px",
                                        backgroundColor: colors.bgColor,
                                        marginTop: "-30px",
                                        position: "relative"
                                    }}>
                                        <Grid sx={{
                                            position: "absolute",
                                            top: "-28px",
                                            left: "-2.5px",
                                            width: "50px",
                                            height: "50px",
                                            borderRadius: "25px",
                                            backgroundColor: colors.btnColor,
                                            display: "flex",
                                            flexDirection: "row",
                                            justifyContent: "center",
                                            alignItems: "center"
                                        }}>
                                            <Image src={item.src} width={25} height={15} objectFit="contain" />
                                        </Grid>
                                    </Grid> : <Image key={index} src={item.src} width={25} height={15} objectFit="contain" />
                                } */}
                                <Image key={index} src={item.src} width={25} height={15} objectFit="contain" />
                            </Grid>
                        );
                    })
                }
            </Grid>
        </Grid>
    );
}

export default BottomNavigationModule;