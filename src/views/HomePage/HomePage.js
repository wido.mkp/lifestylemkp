import React from "react";
import { connect } from "react-redux";
import useStyles from "./Home.module.js";
import Image from "next/image";
import Router, { useRouter } from "next/router";
import Avatar from '@mui/material/Avatar';
import { useTheme } from "@mui/styles";
import Grid from "@mui/material/Grid";
import ListViewModule from "@/components/Listview";
import { Typography } from "@mui/material";
import TextFieldModule from "@/components/TextField/index.js";
import MainLayout from "@/containers/MainLayout/index.js";
import PopularListView from "@/components/PopularListView/index.js";
// import CategoryListView from "@/components/CategoryListView/index.js";
import colors from "src/config/Color.js";
import { POST } from "src/data/apiHelper.js";
import { encode } from "js-base64";
import { DecryptScript } from "@/utils/cryptoHelper.js";

const province = [
    {
        name: "Central Java",
        value: "Jawa Tengah",
        img: "https://salsawisata.b-cdn.net/wp-content/uploads/2022/04/tempat-wisata-di-indonesia-yang-terkenal.jpg"
    },
    {
        name: "East Java",
        value: "Jawa Timur",
        img: "https://thumb.viva.co.id/media/frontend/thumbs3/2018/02/12/5a81470701b0d-suasana-tempat-wisata-telaga-warna-di-kawasan-puncak-kabupaten-bogor_1265_711.jpg"
    },
    {
        name: "West Java",
        value: "Jawa Barat",
        img: "http://assets.kompasiana.com/items/album/2022/07/25/tempat-wisata-malang-62de38e9a51c6f66f35bfc82.jpg"
    },
    {
        name: "Yogyakarta",
        value: "Yogyakarta",
        img: "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2018/11/candi-borobudur.jpg"
    }
]

const region = [
    {
        value: "Places",
        active: true
    },
    {
        value: "Event",
        active: false
    },
    ,];

const HomePage = () => {
    const theme = useTheme();
    const styles = useStyles(theme);
    const [destination, setDestination] = React.useState([])
    const [event, setEvent] = React.useState([]);
    const location = useRouter();
    React.useEffect(() => {
        if (destination.length === 0 && event.length === 0) {
            renderDestination();
            renderEvent();
        }
    }, [destination, event]);

    React.useEffect(() => {
        if (location.asPath) {
            let path = location.asPath?.substring(1)
            if (path !== "[...slug]") {
                let dataDecoder = DecryptScript(path);
                console.log(dataDecoder)
            }
        }
    }, [location.asPath]);

    const renderDestination = async () => {
        let data = {
            billerMerchantKey: process.env.NEXT_PUBLIC_BILLER_MERCHANTKEY,
            tcsCid: "",
            draw: 0,
            limit: 5
        }
        try {
            let res = await POST("/public/biller-merchant-tcs/list/index", data);
            if (res) {
                if (res.success) {
                    setDestination(res.result.listData);
                }
            }
        } catch (error) {
            console.log(error);
        }
    }

    const renderEvent = async () => {
        let data = {
            billerMerchantKey: process.env.NEXT_PUBLIC_BILLER_MERCHANTKEY
        }
        try {
            let res = await POST("/public/biller-merchant-event/list-bydate", data);
            if (res) {
                if (res.success) {
                    setEvent(res.result.listData)
                }
            }
        } catch (error) {
            console.log(error);
        }
    }
    
    const onDetailDestination = (item) => {
        let kCid = encode(item?.cid, true);
        let id = encode(item?.tcsConfig.id, true);
        Router.push({
            pathname: '/product/detail',
            query: {
                idKey: id,
                kCid: kCid
            }
        });
    }

    const onDetailEvent = (item) => {
        let eventCode = item?.eventCode;
        Router.push({
            pathname: '/product/event',
            query: {
                code: eventCode
            }
        });
    }

    return (
        <>
            <MainLayout>
                <Grid style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "flex-end",
                    width: "100%",
                    paddingTop: "30px"
                }}>
                    <Avatar alt="Avatar" src="/assets/images/avatar.png" sx={{
                        width: "49px",
                        height: "52px"
                    }} />
                </Grid>
                <Grid sx={styles.greetingLabel}>
                    <Grid sx={{
                        color: colors.txtColor,
                        fontSize: "0.8rem",
                        textAlign: "left"
                    }}>Hi, John Doe</Grid>
                    <Grid sx={{ display: "flex", flexDirection: "column", textAlign: "left" }}>
                        <Typography mt={2} fontSize="1.2rem" fontWeight="bold" textTransform="uppercase" color="#2B318D">Let’s start</Typography>
                        <Typography fontSize="1.2rem" fontWeight="bold" textTransform="uppercase" color="#2B318D">your journey</Typography>
                    </Grid>
                </Grid>
                <Grid sx={{ position: "relative", width: "100%" }}>
                    <TextFieldModule
                        hiddenLabel={true}
                        variant="outlined"
                        placeholder="Search destination"
                        inputstyle={{
                            fontSize: "0.9rem",
                            width: "100%",
                            height: "50px",
                            borderRadius: "30px",
                            backgroundColor: "white",
                            paddingLeft: "24px",
                            paddingRight: "40px",
                            color: "#3473B9"
                        }}
                    />
                    <Grid sx={{
                        position: "absolute",
                        right: "10px",
                        top: "55%",
                        transform: "translate(-50%, -50%)",
                        cursor: "pointer"

                    }}>
                        <Image src="/assets/images/search.png" width={21} height={21} objectFit="contain" />
                    </Grid>
                </Grid>
                <Grid sx={{
                    // marginLeft: "-25px",
                    // marginRight: "-25px",
                    marginTop: "15px",
                }}>
                </Grid>
                <Grid sx={{
                    display: "flex",
                    flexDirection: "column",
                    width: "100%",
                    marginTop: "-5px"
                }}>
                    {/* <CategoryListView data={category} onClickProduct={() => { }} /> */}
                    <Grid sx={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-between",
                        alignItems: "center",
                        width: "100%",
                        paddingLeft: "10px",
                        paddingRight: "10px",
                        marginTop: "10px"
                    }}>
                        <Grid sx={{
                            display: "flex",
                            flexDirection: "row",
                            paddingLeft: "10px",
                            paddingRight: "10px",
                            justifyContent: "center",
                            alignItems: "center",
                            textDecoration: "none",
                            cursor: "pointer",
                            backgroundColor: colors.bgCompActiveColor,
                            borderRadius: "10px"
                        }}>
                            <Typography sx={{ cursor: "pointer" }} fontSize="0.8rem" fontWeight="bold" color={colors.txtCardColor}>Places</Typography>
                        </Grid>
                        <Typography sx={{ cursor: "pointer" }} fontSize="0.8rem" fontWeight="bold" color={colors.textDescription}>See all</Typography>
                    </Grid>
                    <ListViewModule data={destination} onDetailDestination={onDetailDestination} />
                    <Grid sx={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-between",
                        alignItems: "center",
                        width: "100%",
                        paddingLeft: "10px",
                        paddingRight: "10px",
                        marginTop: "20px"
                    }}>
                        <Grid sx={{
                            display: "flex",
                            flexDirection: "row",
                            paddingLeft: "10px",
                            paddingRight: "10px",
                            justifyContent: "center",
                            alignItems: "center",
                            textDecoration: "none",
                            cursor: "pointer",
                            backgroundColor: colors.bgCompActiveColor,
                            borderRadius: "10px"
                        }}>
                            <Typography sx={{ cursor: "pointer" }} fontSize="0.8rem" fontWeight="bold" color={colors.txtCardColor}>Event</Typography>
                        </Grid>
                        <Typography sx={{ cursor: "pointer" }} fontSize="0.8rem" fontWeight="bold" color={colors.textDescription}>See all</Typography>
                    </Grid>
                    <ListViewModule onDetailEvent={onDetailEvent} isEvent={true} data={event} onDetailDestination={onDetailDestination} />
                    <Grid sx={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-between",
                        marginTop: "10px",
                        paddingLeft: "10px",
                        paddingRight: "10px",
                        marginBottom: "15px"
                    }}>
                        <Typography sx={{ cursor: "pointer" }} mt={2} fontSize="0.8rem" fontWeight="bold" color={colors.bgCompActiveColor}>Province</Typography>
                        <Typography sx={{ cursor: "pointer" }} mt={2} fontSize="0.8rem" fontWeight="bold" color={colors.textDescription}>See all</Typography>
                    </Grid>
                    {
                        province?.map((item, index) => {
                            return (
                                <Grid
                                    key={index}
                                    sx={{
                                        backgroundImage: `url(${item.img})`,
                                        width: "100%",
                                        height: "15vh",
                                        backgroundRepeat: 'no-repeat',
                                        backgroundSize: 'cover ',
                                        borderRadius: 1,
                                        alignSelf: "center",
                                        opacity: 0.5,
                                        position: "relative",
                                        marginBottom: "5px",
                                        cursor: "pointer"
                                    }}>
                                    <Grid sx={{
                                        width: "100%",
                                        height: "100%",
                                        backgroundColor: colors.bgCompActiveColor,
                                        opacity: 0.6,
                                        borderRadius: 1,
                                        paddingLeft: "1.5rem",
                                        display: "flex",
                                        flexDirection: "column",
                                        alignItems: "flex-start",
                                        justifyContent: "center"
                                    }}>
                                    </Grid>
                                    <Grid sx={{
                                        position: "absolute",
                                        top: 0,
                                        bottom: 0,
                                        left: "2rem",
                                        margin: "auto",
                                        display: "flex",
                                        flexDirection: "column",
                                        alignItems: "flex-start",
                                        justifyContent: "center"
                                    }}>
                                        <Typography sx={{ cursor: "pointer", opacity: 1 }} fontSize="0.8rem" fontWeight="bold" color="white">{item.name}</Typography>
                                        <Typography sx={{ cursor: "pointer", opacity: 1 }} fontSize="0.6rem" fontWeight="500" color="white">{item.value}</Typography>
                                    </Grid>
                                </Grid>
                            )
                        })
                    }
                </Grid>
            </MainLayout>
        </>
    );
};

const mapStateToProps = (state) => {
    let { rootReducer } = state;
    return {
        ...rootReducer,
    };
};
export default connect(mapStateToProps)(HomePage);