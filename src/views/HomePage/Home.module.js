import colors from "src/config/Color";

const useStyles = theme => ({
    container: {
        margin: 'auto',
        padding: 0,
        width: '360px',
        height: '100vh',
        textAlign: 'center',
        position: 'relative',
        backgroundColor: colors.bgColor,
        [theme.breakpoints.down('sm')]: {
            width: '100%',
            backgroundColor: colors.bgColor
        },
        paddingLeft: "25px",
        paddingRight: "25px",
        fontFamily: "Poppins",
    },
    greetingLabel: {
        marginTop: "-30px",
        marginBottom: "23px"
    },
    searchWrapper: {
        width: "40px",
        height: "40px",
        borderRadius: "10px",
        backgroundColor: "white",
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },
    historyPayment: {
        width: "95%",
        height: "50px",
        margin: "auto",
        display: "flex",
        flexDirection: "row",
        justifyContent: "end",
        alignItems: "center",
    },
    boxHistory: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        width: "150px",
        cursor: "pointer",
    },
    textHistory: {
        fontSize: "14px",
        color: "#3599e6",
        fontWeight: "bold",
        textDecoration: "underline",
    },
    regActive: {
        position: "absolute",
        width: "8px",
        height: "8px",
        borderRadius: "50%",
        backgroundColor: "#3599e6",
        left: "50%",
        bottom: "5px",
    },
    labelProduct: {
        fontSize: "14px",
        fontWeight: 'bold',
        marginLeft: "20px",
        marginBottom: "20px",
    },
});
export default useStyles;