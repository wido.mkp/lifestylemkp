import React from "react";
import { connect } from "react-redux";
import useStyles from "./BuyProductConfirm.module";
import { useTheme } from "@mui/styles";
import Grid from "@mui/material/Grid";
import { Box, Button, Typography, Divider } from "@mui/material";
import MainLayout from "@/containers/MainLayout/index.js";
import { withRouter } from 'next/router'
import Router from "next/router";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import BuyConfirmCard from "@/components/BuyConfirmCard";
import colors from "src/config/Color";
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { decode, encode } from "js-base64";
import { addLocalStorage } from "@/utils/localStorage";
import { POST } from "src/data/apiHelper";
import BottomSheet from "@/components/BottomSheet";
import convertNumbThousand from "@/utils/convertNumbThousand";
import { message } from "@/utils/message";

const EventProductConfirm = (props) => {
    const theme = useTheme();
    const styles = useStyles(theme);
    const [age, setAge] = React.useState('');
    const [person, setPerson] = React.useState([]);
    const [productTcs, setProduct] = React.useState(null);
    const [passangerSchema, setPassangerSchema] = React.useState(null);
    const [defaultValuePassanger, setDefaultValuePassanger] = React.useState(null);
    const [date, setDate] = React.useState(null);
    const [productSelected, setProductSelected] = React.useState([]);
    const [countPerson, setCountPerson] = React.useState([]);
    const [refresh, setRefresh] = React.useState(false);
    const [openPanel, setOpenPanel] = React.useState(false);
    const key = props.router.query.key;
    const [loading, setLoading] = React.useState(false);
    const [isClickCount, setClickCount] = React.useState(false);

    React.useEffect(() => {
        if (!defaultValuePassanger && !passangerSchema && person.length !== 0) {
            setLoading(true);
            let objectTemp = {};
            let defaultValueTemp = {};
            let totalPassanger = person.length;
            let personTemp = [];
            for (let i = 0; i < totalPassanger; i++) {
                personTemp.push(i);
            }
            setCountPerson(personTemp);
            let productSelectTemp = [];
            for (let i = 0; i < totalPassanger; i++) {
                productSelectTemp.push({
                    passenger: i,
                    productSelected: [],
                });
                objectTemp = {
                    ...objectTemp,
                    [`title_${i}`]: Yup
                        .string('Enter your username')
                        .min(2, 'Enter a valid username')
                        .required('Username is required'),
                    [`identity_${i}`]: Yup
                        .string('Enter your identity')
                        .min(2, 'Enter a valid identity')
                        .required('Identity is required'),
                    [`gender_${i}`]: Yup
                        .string('Enter your gender')
                        .min(2, 'Enter a valid gender')
                        .required('Gender is required'),
                    [`username_${i}`]: Yup
                        .string('Enter your username')
                        .min(8, 'Enter a valid username')
                        .required('Username is required'),
                    [`document_${i}`]: Yup
                        .string('Enter your document')
                        .min(8, 'Enter a valid document')
                        .required('Document is required'),
                    [`telephone_${i}`]: Yup
                        .string('Enter your telephone')
                        .min(8, 'Enter a valid telephone')
                        .required('Telephone is required'),
                    [`email_${i}`]: Yup
                        .string('Enter your email')
                        .email('Enter a valid email')
                        .required('Email is required'),

                };

                defaultValueTemp = {
                    ...defaultValueTemp,
                    [`title_${i}`]: 'Tuan',
                    [`identity_${i}`]: 'KTP',
                    [`gender_${i}`]: 'Male',
                    [`username_${i}`]: '',
                    [`document_${i}`]: '',
                    [`telephone_${i}`]: '',
                    [`email_${i}`]: ''

                };
            }

            objectTemp = {
                ...objectTemp,
            };

            defaultValueTemp = {
                ...defaultValueTemp,
                customerName: '',
                telephone: '',
                email: '',
            };
            setProductSelected(productSelectTemp);
            setPassangerSchema(objectTemp);
            setDefaultValuePassanger(defaultValueTemp);
            setLoading(false);
        }
    }, [defaultValuePassanger, passangerSchema, person]);


    React.useEffect(() => {
        if (person.length === 0 && key && !productTcs) {
            addPerson();
        }
    }, [person, key, productTcs]);

    React.useEffect(() => {
        if (productSelected.length !== 0 && !isClickCount) {
            getProduct()
        }
    }, [productSelected, isClickCount]);

    const handleClickDecrement = (passanger, indexProduct) => {
        setClickCount(true);
        let productTemp = [...productSelected];
        let isSame = productTemp.filter(
            (item) => item.passenger === passanger
        );
        if (isSame.length > 0) {
            isSame.map(async (res) => {
                let productCount = [...res.productSelected];
                let isSameProductCount = productCount.filter(
                    (item) => item.indexProduct === indexProduct
                );
                if (isSameProductCount.length > 0) {
                    if (isSameProductCount[0].value > 0) {
                        let newCount = {};
                        let totalPrice =
                            (isSameProductCount[0].value - 1) *
                            isSameProductCount[0].item?.b2cPrice;
                        newCount = {
                            indexProduct: isSameProductCount[0].indexProduct,
                            value: isSameProductCount[0].value - 1,
                            item: isSameProductCount[0].item,
                            total: totalPrice,
                        };
                        productCount[indexProduct] = newCount;
                        productTemp[passanger] = {
                            passenger: passanger,
                            productSelected: productCount,
                        };
                        setProductSelected(productTemp);
                        // setRefresh(false);
                    }
                }
            });
        }
    };

    const handleClickIncrement = async (passanger, indexProduct) => {
        setClickCount(true);
        let productTemp = [...productSelected];
        let isSame = productTemp.filter(
            (item) => item.passenger === passanger
        );
        if (isSame.length > 0) {
            isSame.map(async (res) => {
                let productCount = [...res.productSelected];
                let isSameProductCount = productCount.filter(
                    (item) => item.indexProduct === indexProduct
                );
                if (isSameProductCount.length > 0) {
                    let newCount = {};
                    let totalPrice =
                        (isSameProductCount[0].value + 1) *
                        isSameProductCount[0].item?.b2cPrice;
                    newCount = {
                        indexProduct: isSameProductCount[0].indexProduct,
                        value: isSameProductCount[0].value + 1,
                        item: isSameProductCount[0].item,
                        total: totalPrice,
                    };
                    productCount[indexProduct] = newCount;
                    productTemp[passanger] = {
                        passenger: passanger,
                        productSelected: productCount,
                    };
                    await setProductSelected(productTemp);
                    // setRefresh(false);
                }
            });
        }
    };

    const getProduct = async () => {
        let passingData = key;
        if (passingData) {
            let decodePassData = decode(passingData);
            let json = JSON.parse(decodePassData);
            let data = {
                billerMerchantKey: process.env.NEXT_PUBLIC_BILLER_MERCHANTKEY,
                eventCode : json?.eventCode,
                eventDetailCode: json?.eventDetailCode,
                eventStartDate: json?.eventStartDate,
                eventEndDate: json?.eventEndDate
            }
            try {
                let res = await POST("/public/mbiller-merchant-event-product/list-byproduct", data);
                if (res) {
                    if (res.success) {
                        let countClickArr = [];
                        let productTemp = productSelected;
                        res?.result?.listProduct?.map((item, index) => {
                            productSelected?.map(async (res, i) => {
                                if (!countClickArr[index]) {
                                    countClickArr.push({
                                        indexProduct: index,
                                        value: 0,
                                        item: item,
                                        total: 0,
                                    });
                                }
                                productTemp[i] = {
                                    passenger: i,
                                    productSelected: countClickArr,
                                };
                                setProductSelected(productTemp);
                            });

                        });
                        setProduct(res?.result?.listProduct);
                    }
                }
            } catch (error) {
                console.log(error);
            }
        }
    }

    const addPerson = () => {
        setLoading(true);
        setPerson([]);
        let personArr = [];
        let { adult } = JSON.parse(decode(key));
        if (Number(adult) !== 0) {
            for (let i = 1; i <= Number(adult); i++) {
                personArr.push({
                    id: i,
                    name: "Adult"
                })
            }
        }
        setPerson(personArr);
        setLoading(false);
        setDate(date);
        // setProduct(product);
    }

    const handleChange = (event) => {
        setAge(event.target.value);
    };

    const goSummary = (values) => {
        let { date, parent } = JSON.parse(decode(key));
        let visitorList = [];
        let bookingList = [];
        countPerson?.map((res) => {
            productSelected?.map((item) => {
                item?.productSelected?.map((count) => {
                    if (item.passenger === res) {
                        if (count.value > 0) {
                            bookingList.push({
                                passenger: item.passenger,
                                item: {
                                    productPrice: count?.item?.b2cPrice,
                                    productVendorCode: count?.item?.productVendorCode,
                                    productQty: count.value,
                                    productFee: count?.item?.serviceFee,
                                    productCode: count?.item?.productCode,
                                    productName: count?.item?.productName,
                                    uniqueProductCode: count?.item?.uniqueProductCode,
                                },
                            });
                        }
                    }
                });
            });
            let booking = [];
            bookingList?.map((book) => {
                if (book.passenger === res) {
                    booking.push(book.item);
                }
            });
            visitorList.push({
                visitorName: values?.[`username_${res}`],
                visitorIdType: values?.[`identity_${res}`],
                visitorId: values?.[`document_${res}`],
                visitorPhone: values?.[`telephone_${res}`],
                visitorEmail: values?.[`email_${res}`],
                visitorGender: values?.[`gender_${res}`],
                bookingList: booking,
            });
        });
        let getTotalPrice = 0;
        productSelected?.map((item) => {
            item?.productSelected?.map((res) => {
                getTotalPrice += res?.total;
            });
        });
        let body = {
            billerMerchantCode: "LMMKP",
            billerRef: "billerref-101",
            billerPaymentMethod: "BILLER DEPOSIT",
            customerEmail: "yonathan@mkpmobile.com",
            customerPhone: "083838091683",
            customerName: "Yonathan P",
            detail: [
                {
                    tcCID: parent.cid,
                    tcVendorCode: parent.vendorCode,
                    tcName: parent.name,
                    tcHirarki: parent.hirarki,
                    bookingDate: date,
                    visitorList: visitorList,
                },
            ],
        };
        console.log(body)
        // addLocalStorage({ cart: cartDt });
        // Router.push({
        //     pathname: "/summary",
        //     query: {
        //         key: encode(JSON.stringify(cartDt))
        //     }
        // });
    }

    if (loading) {
        return null;
    } else {
        let getTotalPrice = 0;
        let getTotalServicePrice = 0;
        productSelected?.map((item) => {
            item?.productSelected?.map((res) => {
                getTotalPrice += res?.total;
                getTotalServicePrice += res?.item?.serviceFee;
            });
        });

        return (
            <>
                <MainLayout disableNav={true}>
                    <Grid sx={{
                        display: "flex",
                        flexDirection: "row",
                        marginTop: "35px",
                        alignItems: "center",
                        cursor: "pointer"
                    }}
                        onClick={() => Router.back()}
                    >
                        <ArrowBackIcon fontSize="0.9rem" />
                        <Typography fontSize="0.9rem" marginLeft="5px" fontWeight="500">Back</Typography>
                    </Grid>
                    <Grid sx={{
                        display: "flex",
                        flexDirection: "column",
                        marginTop: "40px",
                        paddingTop: "20px",
                        textAlign: "left"
                    }}>
                        <Typography fontSize="0.9rem" fontWeight="bold">Details</Typography>
                        <Grid
                            sx={{
                                width: "100%",
                                borderRadius: "13px",
                                backgroundColor: colors.bgCardColor,
                                display: "flex",
                                justifyContent: "flex-start",
                                flexDirection: "column",
                                alignItems: "flex-start",
                                padding: "20px",
                                marginTop: "20px"
                            }}>
                            <Grid sx={{
                                display: "flex",
                                width: "100%",
                                flexDirection: "row",
                                alignItems: "center",
                                justifyContent: "space-between",
                            }}>
                                <Typography sx={{ color: colors.txtCompActiveColor }} fontSize="0.9rem" fontWeight="bold">Pemesan</Typography>
                                <Typography sx={{ color: colors.txtCompActiveColor }} fontSize="0.7rem">{date}</Typography>
                            </Grid>
                            <Typography sx={{ color: colors.txtCompActiveColor }} fontSize="1.5rem" fontWeight="bold">John Doe B</Typography>
                            <Typography sx={{ color: colors.txtCompActiveColor }} fontSize="0.9rem" fontWeight="bold">No KTP  : 33773435xxx </Typography>
                            <Typography sx={{ color: colors.txtCompActiveColor }} fontSize="0.9rem" fontWeight="bold">No HP   : 085432435xxx </Typography>
                        </Grid>
                        {
                            defaultValuePassanger && passangerSchema ?
                                <Formik
                                    initialValues={{
                                        ...defaultValuePassanger,
                                    }}
                                    validationSchema={Yup.object().shape(passangerSchema)}
                                    onSubmit={(values) => {
                                        let getCountItem = [];
                                        let getCheckPassanger = -1;
                                        productSelected?.map((item) => {
                                            item?.productSelected?.map((res) => {
                                                if (res.value > 0) {
                                                    getCheckPassanger = item.passenger;
                                                }
                                            });
                                            if (getCheckPassanger !== -1) {
                                                let checkIsSame = false;
                                                getCountItem?.map((item) => {
                                                    if (item.passenger === getCheckPassanger) {
                                                        checkIsSame = true;
                                                    }
                                                });
                                                if (!checkIsSame) {
                                                    getCountItem.push({
                                                        passenger: getCheckPassanger,
                                                    });
                                                }
                                            }
                                        });
                                        if (getCountItem.length !== countPerson.length) {
                                            message('error', 'Anda belum memilih produk!');
                                        } else {
                                            goSummary(values)
                                        }
                                    }}
                                >
                                    {({ errors, touched, setFieldValue, handleChange, values }) => {
                                        return (
                                            <Form>
                                                {
                                                    person.map((item, index) => {
                                                        return (
                                                            <Grid key={index} sx={{ marginBottom: "-20px" }}>
                                                                <BuyConfirmCard
                                                                    product={productTcs}
                                                                    setFieldValue={setFieldValue}
                                                                    errors={errors}
                                                                    values={values}
                                                                    touched={touched}
                                                                    index={index}
                                                                    title="Pengunjung"
                                                                    id={item.id}
                                                                    age={age}
                                                                    productSelected={productSelected}
                                                                    handleClickDecrement={handleClickDecrement}
                                                                    handleClickIncrement={handleClickIncrement}
                                                                    handleChange={handleChange}
                                                                    open={index === 0 ? true : false}
                                                                />
                                                            </Grid>
                                                        );
                                                    })
                                                }
                                                <Grid sx={{
                                                    position: "fixed",
                                                    bottom: 0,
                                                    left: 0,
                                                    right: 0,
                                                    marginLeft: "auto",
                                                    marginRight: "auto",
                                                    width: "360px",
                                                    [theme.breakpoints.down('md')]: {
                                                        width: "100%",
                                                    },
                                                    display: "flex",
                                                    flexDirection: "row",
                                                    justifyContent: "space-between",
                                                    alignItems: "center",
                                                    paddingLeft: "25px",
                                                    paddingRight: "25px",
                                                    paddingTop: "10px",
                                                    paddingBottom: "10px",
                                                    backgroundColor: colors.bgPassangerModal
                                                }}>
                                                    <Button
                                                        onClick={() => setOpenPanel(true)}
                                                        type="button"
                                                        sx={{
                                                            backgroundColor: "#228FC7",
                                                            fontSize: "0.9rem",
                                                            textTransform: "capitalize",
                                                            borderRadius: "13px",
                                                            width: "45%",
                                                            height: "31px",
                                                            color: colors.txtCompActiveColor,
                                                            "&:hover": {
                                                                color: 'gray',
                                                                backgroundColor: 'lightblue'
                                                            }
                                                        }}
                                                    >Lihat Pesanan</Button>
                                                    <Button
                                                        type="submit"
                                                        sx={{
                                                            backgroundColor: "#228FC7",
                                                            fontSize: "0.9rem",
                                                            textTransform: "capitalize",
                                                            borderRadius: "13px",
                                                            width: "45%",
                                                            height: "31px",
                                                            color: colors.txtCompActiveColor,
                                                            "&:hover": {
                                                                color: 'gray',
                                                                backgroundColor: 'lightblue'
                                                            }
                                                        }}
                                                    >Next</Button>
                                                </Grid>
                                            </Form>
                                        )
                                    }}
                                </Formik> : null
                        }
                    </Grid>
                    <BottomSheet isOpen={openPanel} onChange={setOpenPanel}>
                        <Grid sx={{
                            height: "80%",
                            display: "flex",
                            flexDirection: "column",
                            paddingLeft: "25px",
                            paddingRight: "25px",
                            paddingTop: "25px"
                        }}>
                            <Typography sx={{ color: "black" }} fontSize="1.2rem" fontWeight="bold" textTransform="capitalize">Detail pesanan anda</Typography>
                            <Grid sx={{
                                width: "100%",
                                display: "flex",
                                flexDirection: "column",
                                justifyContent: "center",
                                alignItems: "flex-start",
                                marginTop: "20px",
                                marginBottom: "20px"
                            }}>
                                <Grid sx={{
                                    display: "flex",
                                    width: "100%",
                                    flexDirection: "row",
                                    justifyContent: "space-between"
                                }}>
                                    <Typography sx={{ color: "black" }} fontSize="1.1rem" textTransform="capitalize">Tanggal Booking</Typography>
                                    <Typography sx={{ color: "black" }} fontSize="0.9rem" textTransform="capitalize">{date}</Typography>
                                </Grid>
                                <Divider sx={{
                                    width: "100%",
                                    marginTop: "10px",
                                    marginBottom: "10px"
                                }} />
                                {productSelected?.map((item, index) => {
                                    return (
                                        <div style={{
                                            width: "100%",
                                            display: "flex",
                                            flexDirection: "column",
                                            justifyContent: "center",
                                            alignItems: "flex-start"
                                        }} key={index}>
                                            <Typography sx={{ color: "black", marginBottom: "5px" }} fontSize="1.1rem" fontWeight="bold" textTransform="capitalize">Pengunjung {index + 1}</Typography>
                                            {item?.productSelected?.map((res, j) => {
                                                if (res.value > 0) {
                                                    return (
                                                        <Grid
                                                            key={j}
                                                            sx={{
                                                                display: "flex",
                                                                width: "100%",
                                                                flexDirection: "row",
                                                                justifyContent: "space-between"
                                                            }}>
                                                            <Typography sx={{ color: "black" }} fontSize="1.1rem">{res?.item?.productName} x {res?.value}</Typography>
                                                            <Typography sx={{ color: "black" }} fontSize="1.1rem" textTransform="capitalize">{
                                                                "Rp. " +
                                                                convertNumbThousand(
                                                                    res?.value * res?.item?.b2cPrice
                                                                )}
                                                            </Typography>
                                                        </Grid>
                                                    )
                                                }
                                            })}
                                        </div>
                                    )
                                })}
                                <Divider sx={{
                                    width: "100%",
                                    marginTop: "10px",
                                    marginBottom: "10px"
                                }} />
                                <Grid sx={{
                                    display: "flex",
                                    width: "100%",
                                    flexDirection: "row",
                                    justifyContent: "space-between"
                                }}>
                                    <Typography sx={{ color: "black" }} fontSize="1.1rem" textTransform="capitalize">Total</Typography>
                                    <Typography sx={{ color: "black" }} fontSize="1.1rem" fontWeight="bold" textTransform="capitalize">Rp {convertNumbThousand(getTotalPrice)}</Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                    </BottomSheet>
                </MainLayout>
            </>
        );
    }
};

const mapStateToProps = (state) => {
    let { rootReducer } = state;
    return {
        ...rootReducer,
    };
};
export default withRouter(connect(mapStateToProps)(EventProductConfirm));