import { Button, Grid, Typography } from "@mui/material";
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import PersonIcon from '@mui/icons-material/Person';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import PassangerOption from "@/components/PassengerOption";
import React from "react";
import colors from "src/config/Color";
import DatePicker from "@hassanmojab/react-modern-calendar-datepicker";
import DatePickerModal from "@/components/Filter/DataPickerModal";
import moment from "moment";
import { toast } from "react-toastify";

const BuyProductTab = ({ nextBuy, styles, setDatePickerParent }) => {
    const [open, setOpen] = React.useState(false);
    const [dataPassanger, setDataPassanger] = React.useState({});
    const [isDate, setDate] = React.useState(false);
    const [selectedDay, setSelectedDay] = React.useState("");
    const [date, setDatePicker] = React.useState("");
    const submitDateForm = (item) => {
        setDatePickerParent(item);
        setDatePicker(item);
        setDate(false);
    }

    return (
        <Grid sx={{ marginBottom: "20px" }}>
            <Typography fontSize="0.8rem" color={colors.bgCompActiveColor} fontWeight="bold">
                Date
            </Typography>
            <Typography fontSize="0.7rem" color={colors.labelCompInactiveColor}>
                Choose your departure date
            </Typography>
            <Grid
                sx={{
                    width: "100%",
                    height: "51px",
                    borderRadius: "13px",
                    backgroundColor: colors.bgCompActiveColor,
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    paddingLeft: "20px",
                    marginTop: "20px"
                }}
                onClick={() => setDate(true)}
            >
                <CalendarMonthIcon sx={{ color: colors.txtCardColor, fontSize: "1.4rem" }} />
                <Typography sx={{ color: colors.txtCardColor }} fontSize="0.9rem" marginLeft="10px" fontWeight="bold">{date !== "" ? date : "Silahkan pilih tanggal"}</Typography>
            </Grid>
            <Typography fontSize="0.8rem" color={colors.bgCompActiveColor} fontWeight="bold" marginTop="25px">
                People
            </Typography>
            <Typography fontSize="0.7rem" color={colors.labelCompInactiveColor}>
                Number of people in your group
            </Typography>
            <Grid
                sx={{
                    width: "100%",
                    height: "51px",
                    borderRadius: "13px",
                    backgroundColor: colors.bgCompActiveColor,
                    display: "flex",
                    justifyContent: "space-between",
                    flexDirection: "row",
                    alignItems: "center",
                    paddingLeft: "20px",
                    paddingRight: "15px",
                    marginTop: "20px"
                }}
                onClick={() => setOpen(true)}
            >
                <Grid sx={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                    width: "100%"
                }}>
                    {
                        Object.keys(dataPassanger).length === 0 ?
                            <>
                                <Typography sx={{ color: colors.txtCardColor }} fontSize="0.9rem" marginLeft="10px" fontWeight="bold">
                                    Select Passenger
                                </Typography>
                                <KeyboardArrowDownIcon sx={{ color: colors.txtCardColor, fontSize: "1.4rem" }} />
                            </> :
                            <>
                                <PersonIcon sx={{ color: colors.txtCardColor, fontSize: "1.4rem" }} />
                                <Typography sx={{ color: colors.txtCardColor }} fontSize="0.9rem" marginLeft="10px" fontWeight="bold">
                                    {
                                        dataPassanger?.adult !== 0 ? dataPassanger.adult + " Adult" : null
                                    }
                                    {
                                        dataPassanger?.adult === 0 && dataPassanger?.kid !== 0 ? "" :
                                            dataPassanger?.adult !== 0 && dataPassanger?.kid === 0 ? "" : " , "
                                    }
                                    {
                                        dataPassanger?.kid !== 0 ? dataPassanger.kid + " Kid" : null
                                    }
                                </Typography>
                            </>
                    }
                </Grid>
            </Grid>
            <Grid sx={{
                width: "100%",
                display: "flex",
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center"
            }}>
                <Button sx={{
                    backgroundColor: "#228FC7",
                    fontSize: "0.9rem",
                    textTransform: "capitalize",
                    borderRadius: "13px",
                    width: "199px",
                    height: "51px",
                    color: colors.txtCardColor,
                    marginTop: "61px",
                    "&:hover": {
                        color: 'gray',
                        backgroundColor: 'lightblue'
                    }
                }}
                    onClick={() => {
                        if (date === "") {
                            toast.warning("Silahkan pilih tanggal!", {
                                position: "bottom-center",
                                autoClose: 5000,
                                hideProgressBar: false,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true,
                                progress: undefined,
                            })
                        } else if (Object.keys(dataPassanger).length === 0) {
                            toast.warning("Silahkan pilih pengunjung!", {
                                position: "bottom-center",
                                autoClose: 5000,
                                hideProgressBar: false,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true,
                                progress: undefined,
                            })
                        } else {
                            if (dataPassanger.adult === 0 && dataPassanger.kid === 0) {
                                toast.warning("Silahkan pilih pengunjung!", {
                                    position: "bottom-center",
                                    autoClose: 5000,
                                    hideProgressBar: false,
                                    closeOnClick: true,
                                    pauseOnHover: true,
                                    draggable: true,
                                    progress: undefined,
                                })
                            } else if (dataPassanger.adult === 0 && dataPassanger.kid !== 0) {
                                toast.warning("Harus memilih pengunjung dewasa!", {
                                    position: "bottom-center",
                                    autoClose: 5000,
                                    hideProgressBar: false,
                                    closeOnClick: true,
                                    pauseOnHover: true,
                                    draggable: true,
                                    progress: undefined,
                                });
                            } else {
                                nextBuy(dataPassanger)
                            }
                        }
                    }}
                >Next</Button>
            </Grid>
            <PassangerOption open={open} handleClose={() => setOpen(false)} setDataPassanger={setDataPassanger} />
            <DatePickerModal submitForm={submitDateForm} open={isDate} handleClose={() => setDate(false)} selectedDay={selectedDay} setSelectedDay={setSelectedDay} />
        </Grid>
    );
}

export default BuyProductTab;