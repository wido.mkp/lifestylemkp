const useStyles = theme => ({
    container: {
        margin: 'auto',
        padding: 0,
        width: '360px',
        height: '100vh',
        textAlign: 'center',
        position: 'relative',
        backgroundColor: '#E9E9E9',
        [theme.breakpoints.down('sm')]: {
            width: '100%',
            backgroundColor: '#E9E9E9'
        },
        paddingLeft: "25px",
        paddingRight: "25px",
        fontFamily: "Poppins",
    },
    filterTab: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
        width: "100%",
        height: "51px",
        backgroundColor: "#2C2C2C",
        borderRadius: "13px",
        marginTop: "43px",
        position: "sticky",
        top: "10px",
        zIndex: 1000
    },
});
export default useStyles;