import { Grid, Typography } from "@mui/material";
import colors from "src/config/Color";
import getSrc from "get-src";

const DescriptionTab = ({ data, productProfile }) => {
    return (
        <Grid sx={{ marginBottom: "10px" }}>
            <div style={{
                fontSize: "0.7rem",
                margin: 0,
                padding: 0,
            }} dangerouslySetInnerHTML={{ __html: productProfile?.tcsConfig?.description }} />
            <Typography marginTop="10px" fontSize="0.8rem" color={colors.textDescription} fontWeight="bold">
                Address
            </Typography>
            <Typography fontSize="0.7rem" color={colors.textDescription}>
                {productProfile?.address} , {data?.phone || "-"}
            </Typography>
            <Typography marginTop="10px" fontSize="0.8rem" color={colors.textDescription} fontWeight="bold">
                Location
            </Typography>
            {
                data && data.embedMap ?
                    <iframe
                        src={getSrc(data.embedMap)}
                        width="100%"
                        height="300"
                        loading="lazy"
                        style={{
                            pointerEvents: "auto",
                            border: 0
                        }}
                    /> : null
            }
        </Grid>
    );
}

export default DescriptionTab;