import React from "react";
import { connect } from "react-redux";
import useStyles from "./DetailProduct.module";
import { useTheme } from "@mui/styles";
import Grid from "@mui/material/Grid";
import { Typography } from "@mui/material";
import MainLayout from "@/containers/MainLayout/index.js";
import { withRouter } from 'next/router'
import Router from "next/router";
import SlideShowModule from "@/components/Slideshow";
import DescriptionTab from "./DescriptionTab";
import BuyProductTab from "./BuyProductTab";
import colors from "src/config/Color";
import { decode, encode } from "js-base64";
import { POST } from "src/data/apiHelper";
import EventCard from "./EventCard";
import PassangerOption from "@/components/PassengerOption";
import { toast } from "react-toastify";

const category = ["Description", "Buy Ticket", "Reviews"]
const DetailEvent = (props) => {
    const theme = useTheme();
    const styles = useStyles(theme);
    const [isActiveCategory, setActiveCategory] = React.useState("Description");
    const [tcsData, setTcsData] = React.useState(null);
    const [product, setProduct] = React.useState(null);
    const [imageGallery, setImageGallery] = React.useState([]);
    const [date, setDate] = React.useState("");
    const idKey = props.router.query.code;

    const onTab = (value) => {
        setActiveCategory(value);
    }

    React.useEffect(() => {
        if (Router.isReady && idKey) {
            if (!tcsData) {
                getDetailTcsProfile();
            }
        }
    }, [tcsData, idKey])

    const getDetailTcsProfile = async () => {
        if (idKey) {
            let data = {
                billerMerchantKey: process.env.NEXT_PUBLIC_BILLER_MERCHANTKEY,
                eventStartDate: "",
                eventEndDate: "",
                eventCode: idKey
            }
            try {
                let res = await POST("/public/biller-merchant-event-detail/list-byevent", data);
                if (res) {
                    if (res.success) {
                        let result = res.result;
                        let imagesArr = [];
                        result?.map((item) => {
                            item?.imageGallery?.map((image) => {
                                imagesArr.push(image);
                            })
                        })
                        setImageGallery(imagesArr);
                        setTcsData(result)
                    }
                }
            } catch (error) {
                console.log(error);
            }
        }
    }

    const nextBuy = (data) => {
        let result = {
            adult: data?.adult,
            eventCode: data?.parent?.eventCode,
            eventDetailCode: data?.parent?.eventDetailCode,
            eventStartDate: data?.parent?.eventDetailSchedule[0]?.eventStartDate,
            eventEndDate: data?.parent?.eventDetailSchedule[0]?.eventEndDate
        }
        if (data?.adult === 0) {
            toast.warning("Silahkan pilih pengunjung!", {
                position: "bottom-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            })
        } else {
            Router.push({
                pathname: '/product/event_product',
                query: {
                    key: encode(JSON.stringify(result))
                }
            });
        }
    }
    return (
        <>
            <MainLayout usePadding={false}>
                <SlideShowModule styles={styles} data={imageGallery} />
                <Grid sx={{
                    display: "flex",
                    flexDirection: "column",
                    paddingLeft: "25px",
                    paddingRight: "25px",
                    textAlign: "left"
                }}>
                    {
                        tcsData?.map((item, index) => {
                            return <EventCard key={index} data={item} nextBuy={nextBuy} />
                        })
                    }
                </Grid>
            </MainLayout>
        </>
    );
};

const mapStateToProps = (state) => {
    let { rootReducer } = state;
    return {
        ...rootReducer,
    };
};
export default withRouter(connect(mapStateToProps)(DetailEvent));