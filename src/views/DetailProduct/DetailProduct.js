import React from "react";
import { connect } from "react-redux";
import useStyles from "./DetailProduct.module";
import { useTheme } from "@mui/styles";
import Grid from "@mui/material/Grid";
import { Typography } from "@mui/material";
import MainLayout from "@/containers/MainLayout/index.js";
import { withRouter } from 'next/router'
import Router from "next/router";
import SlideShowModule from "@/components/Slideshow";
import DescriptionTab from "./DescriptionTab";
import BuyProductTab from "./BuyProductTab";
import colors from "src/config/Color";
import { decode, encode } from "js-base64";
import { POST } from "src/data/apiHelper";
import { getLocalStorage } from "@/utils/localStorage";

const category = ["Description", "Buy Ticket", "Reviews"]
const DetailProduct = (props) => {
    const theme = useTheme();
    const styles = useStyles(theme);
    const [isActiveCategory, setActiveCategory] = React.useState("Description");
    const [tcsData, setTcsData] = React.useState(null);
    const [product, setProduct] = React.useState(null);
    const [date, setDate] = React.useState("");
    const idKey = props.router.query.idKey;

    const onTab = (value) => {
        setActiveCategory(value);
    }

    React.useEffect(() => {
        if (Router.isReady && idKey) {
            if (!tcsData) {
                getDetailTcsProfile();
            }
        }
    }, [tcsData, idKey])

    const getDetailTcsProfile = async () => {
        if (idKey) {
            let id = decode(idKey);
            let data = {
                billerMerchantKey: process.env.NEXT_PUBLIC_BILLER_MERCHANTKEY,
                tcsCid: ""
            }
            try {
                let res = await POST("/public/biller-merchant-tcs/list/index", data);
                if (res) {
                    if (res.success) {
                        let result = res.result.listData;
                        result?.map(async (item) => {
                            if (item.tcsConfig.id === id) {
                                setProduct(item);
                                await setTcsData(item);
                            }
                        })
                    }
                }
            } catch (error) {
                console.log(error);
            }
        }
    }

    const nextBuy = (data) => {
        let idKeyPath = Router.router.state.asPath.split("&")[0];
        let kCidPath = Router.router.state.asPath.split("&")[1];
        let id = idKeyPath.split("idKey=")[1];
        let kCid = kCidPath.split("kCid=")[1];
        let parent = {
            cid: product?.cid,
            vendorCode: product?.tcVendorCode,
            name: product?.name,
            hirarki: product?.hirarki
        }
        let result = {
            adult: data?.adult,
            kid: data?.kid,
            kCid: kCid,
            id: id,
            date: date,
            parent: parent
        }
        Router.push({
            pathname: '/product/buy',
            query: {
                key: encode(JSON.stringify(result))
            }
        });
    }
    return (
        <>
            <MainLayout usePadding={false}>
                <SlideShowModule styles={styles} data={product?.tcsConfig?.imageGallery} />
                <Grid sx={{
                    display: "flex",
                    flexDirection: "column",
                    paddingLeft: "25px",
                    paddingRight: "25px",
                    textAlign: "left",
                    marginTop: "-80px"
                }}>
                    <Grid sx={{
                        display: "flex",
                        flexDirection: "row",
                        width: "200px",
                        whiteSpace: "initial",
                    }}>
                        <Typography fontSize="1.5rem" fontWeight="500" textTransform="uppercase">{tcsData?.name.toLowerCase()}</Typography>
                    </Grid>
                    <Typography fontSize="0.8rem" color={colors.labelCompInactiveColor} fontWeight="500" textTransform="uppercase">IDR {product?.minPrice} - {product?.maxPrice} / {product?.tcsCategoryName}</Typography>
                    <Grid sx={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-between",
                        marginTop: "20px",
                    }}>
                        {
                            category.map((item, index) => {
                                return (
                                    isActiveCategory === item ?
                                        <Grid key={index} sx={{
                                            display: "inline-block",
                                            textAlign: "center",
                                            padding: "5px 15px 5px 15px",
                                            textDecoration: "none",
                                            color: colors.txtCompActiveColor,
                                            fontSize: "0.8rem",
                                            cursor: "pointer",
                                            position: "relative",
                                            backgroundColor: colors.bgCompActiveColor,
                                            borderRadius: "28px",
                                        }}>
                                            {item}
                                        </Grid> :
                                        <Grid key={index} sx={{
                                            display: "inline-block",
                                            textAlign: "center",
                                            textDecoration: "none",
                                            color: colors.labelCompInactiveColor,
                                            fontSize: "0.8rem",
                                            padding: "5px 15px 5px 15px",
                                            cursor: "pointer",
                                            position: "relative",
                                        }} onClick={() => onTab(item)}>
                                            {item}
                                        </Grid>
                                )
                            })
                        }
                    </Grid>
                    <Grid sx={{
                        marginTop: "30px"
                    }}>
                        {
                            isActiveCategory === "Description" ?
                                <DescriptionTab data={tcsData?.tcsConfig} productProfile={product} /> : isActiveCategory === "Buy Ticket" ?
                                    <BuyProductTab styles={styles} nextBuy={nextBuy} setDatePickerParent={(item) => setDate(item)} /> : null
                        }
                    </Grid>
                </Grid>
            </MainLayout>
        </>
    );
};

const mapStateToProps = (state) => {
    let { rootReducer } = state;
    return {
        ...rootReducer,
    };
};
export default withRouter(connect(mapStateToProps)(DetailProduct));