import * as React from 'react';
import { Button, Collapse, Grid, Typography } from "@mui/material";
import Divider from '@mui/material/Divider';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import PassangerOption from "@/components/PassengerOption";
import colors from 'src/config/Color';
import getSrc from "get-src";

export default function EventCard({ data, nextBuy }) {
    const [isCollapse, setCollapse] = React.useState(false);
    const [open, setOpen] = React.useState(false);
    const [dataPassanger, setDataPassanger] = React.useState({});
    return (
        <div>
            <Grid sx={{
                width: "100%",
                height: "auto",
                borderRadius: "10px",
                borderStyle: "solid",
                borderWidth: "0.1px",
                borderColor: "#d4d3cf",
                boxShadow: "1px 1px 5px 0px rgba(199,187,187,0.75)",
                '&::-webkit-box-shadow': "1px 1px 5px 0px rgba(199,187,187,0.75)",
                '&::-moz-box-shadow': "1px 1px 5px 0px rgba(199,187,187,0.75)",
                display: "flex",
                flexDirection: "column",
                padding: '10px',
                alignItems: "flex-start",
                marginBottom: "10px"
            }}>
                {
                    data && data.imageBanner &&
                    <img
                        src={data?.imageBanner?.[0]}
                        style={{
                            width: "80px",
                            height: "80px",
                            borderRadius: "50%",
                            objectFit: "cover"
                        }}
                    />
                }
                <Grid sx={{
                    marginTop: "15px",
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "flex-start"
                }}
                >
                    <Typography fontSize="1.2rem" fontWeight="bold" color={colors.textDescription}>{data?.eventDetailName}</Typography>
                    <Typography fontSize="0.9rem" color="gray">
                        {
                            data?.eventDetailSchedule?.length !== 0 && data?.eventDetailSchedule?.[0]?.eventStartDate
                        }
                        {"   "}-{"   "}
                        {
                            data?.eventDetailSchedule?.length !== 0 && data?.eventDetailSchedule?.[0]?.eventEndDate
                        }
                    </Typography>
                    <Grid sx={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-between",
                        alignItems: "center",
                        marginTop: "20px",
                        width: "100%"
                    }}>
                        <Button sx={{
                            backgroundColor: colors.bgCardColor,
                            borderRadius: "10px",
                            width: "100px"
                        }}
                            onClick={() => setOpen(true)}
                        >
                            <Typography fontSize="0.7rem" textTransform="capitalize" color={colors.txtCardColor}>Pesan</Typography>
                        </Button>
                        <KeyboardArrowDownIcon onClick={() => setCollapse(!isCollapse)} />
                    </Grid>
                </Grid>
                <Collapse in={isCollapse}>
                    <Grid sx={{
                        marginTop: "20px",
                        display: "flex",
                        flexDirection: "column",
                        width: "100%"
                    }}>
                        <Typography fontSize="0.9rem" color={colors.textDescription} fontWeight="bold" marginTop="10px">Deskripsi</Typography>
                        <Typography fontSize="0.9rem" color={colors.textDescription}>{data?.eventDetailDescription}</Typography>
                        <Typography fontSize="0.9rem" color={colors.textDescription} fontWeight="bold" marginTop="10px">Jadwal</Typography>
                        <Typography fontSize="0.9rem" color={colors.textDescription}>
                            {
                                data?.eventDetailSchedule?.length !== 0 && data?.eventDetailSchedule?.[0]?.eventStartDate
                            }
                            {"   "}-{"   "}
                            {
                                data?.eventDetailSchedule?.length !== 0 && data?.eventDetailSchedule?.[0]?.eventEndDate
                            }
                        </Typography>
                        <Typography fontSize="0.9rem" color={colors.textDescription} fontWeight="bold" marginTop="10px" marginBottom="10px">Lokasi</Typography>
                        {
                            data && data?.embedMap &&
                            <iframe
                                src={getSrc(data?.embedMap)}
                                width="100%"
                                height="300"
                                loading="lazy"
                                style={{
                                    pointerEvents: "auto",
                                    border: 0
                                }}
                            />
                        }
                    </Grid>
                    <Grid sx={{
                        display: "flex",
                        flexDirection: "row",
                        width: "100%",
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                        onClick={() => setCollapse(!isCollapse)}
                    >
                        <KeyboardArrowUpIcon sx={{ fontSize: "0.8rem", color: colors.txtCompActiveColor, marginBottom: "5px" }} />
                    </Grid>
                </Collapse>
            </Grid>
            <PassangerOption handleConfirmToCheckout={nextBuy} data={data} isEvent={true} open={open} handleClose={() => setOpen(false)} setDataPassanger={setDataPassanger} />
        </div>
    );
}