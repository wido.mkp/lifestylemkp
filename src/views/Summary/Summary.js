import React from "react";
import { connect } from "react-redux";
import useStyles from "./Summary.module";
import { useTheme } from "@mui/styles";
import Grid from "@mui/material/Grid";
import { Button, Typography } from "@mui/material";
import MainLayout from "@/containers/MainLayout/index.js";
import { withRouter } from 'next/router'
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Router from "next/router";
import SummaryCard from "@/components/SummaryCard";
import colors from "src/config/Color";

const product = [1, 2, 3, 4, 5, 6, 7, 8];
const Summary = (props) => {
    const theme = useTheme();
    const styles = useStyles(theme);

    return (
        <>
            <MainLayout disableNav={true} usePadding={false}>
                <Grid sx={{
                    display: "flex",
                    flexDirection: "row",
                    marginTop: "35px",
                    alignItems: "center",
                    cursor: "pointer",
                    paddingLeft: "25px",
                    paddingRight: "25px"
                }}
                    onClick={() => Router.back()}
                >
                    <ArrowBackIcon fontSize="0.9rem" />
                    <Typography fontSize="0.9rem" marginLeft="5px" fontWeight="500">Back</Typography>
                </Grid>
                <Grid sx={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "flex-start",
                    width: "100%",
                    height: "81px",
                    backgroundColor: colors.bgCardColor,
                    marginTop: "30px",
                    paddingLeft: "40px",
                    position: "sticky",
                    top: "0px"
                }}>
                    <Typography fontSize="0.7rem" color={colors.bgColor} fontWeight="bold">Total pembayaran</Typography>
                    <Typography fontSize="1.7rem" marginTop="5px" color={colors.bgColor} fontWeight="bold">IDR 120.000</Typography>
                </Grid>
                <Grid sx={{
                    display: "flex",
                    flexDirection: "column",
                    paddingLeft: "25px",
                    paddingRight: "25px",
                    textAlign: "left",
                    marginTop: "16px"
                }}
                >
                    <Typography marginLeft="10px" fontSize="0.7rem" color={colors.bgCompActiveColor} fontWeight="bold">Transaction List</Typography>
                    <SummaryCard />
                    <SummaryCard />
                    <Grid sx={{
                        display: "flex",
                        flexDirection: "row",
                        width: "100%",
                        justifyContent: "center",
                        alignItems: "center"
                    }}>
                        <Button sx={{
                            backgroundColor: colors.btnColor,
                            fontSize: "0.9rem",
                            textTransform: "capitalize",
                            borderRadius: "13px",
                            width: "199px",
                            height: "51px",
                            color: colors.txtCompActiveColor,
                            marginTop: "61px",
                        }}
                        >Lanjut Pembayaran</Button>
                    </Grid>
                </Grid>
            </MainLayout>
        </>
    );
};

const mapStateToProps = (state) => {
    let { rootReducer } = state;
    return {
        ...rootReducer,
    };
};
export default withRouter(connect(mapStateToProps)(Summary));