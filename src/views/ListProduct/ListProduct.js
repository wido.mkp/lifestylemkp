import React from "react";
import { connect } from "react-redux";
import useStyles from "./ListProduct.module";
import { useTheme } from "@mui/styles";
import Grid from "@mui/material/Grid";
import { Button, Typography } from "@mui/material";
import MainLayout from "@/containers/MainLayout/index.js";
import { withRouter } from 'next/router'
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import SortIcon from '@mui/icons-material/Sort';
import FilterAltIcon from '@mui/icons-material/FilterAlt';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import Router from "next/router";
import ListProductCard from "@/components/ListProductCard";
import Sort from "@/components/Filter/Sort";
import Filter from "@/components/Filter/Filter";
import colors from "src/config/Color";
import { POST } from "src/data/apiHelper";
import { encode, decode } from "js-base64";
import { addLocalStorage } from "@/utils/localStorage";

const ListProduct = (props) => {
    const theme = useTheme();
    const styles = useStyles(theme);
    const [open, setOpen] = React.useState(false);
    const [sort, setSort] = React.useState("");
    const [filter, setFilter] = React.useState("");
    const [isFilter, openFilter] = React.useState(false);
    const [product, setProduct] = React.useState([]);
    const [parentProduct, setParentProduct] = React.useState([])
    const [adult, setAdult] = React.useState(0);
    const [kid, setKid] = React.useState(0);
    const key = Router.router?.state?.query?.key;
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    React.useEffect(() => {
        if (product.length === 0 && parentProduct.length === 0 && key) {
            getProduct();
        }
    }, [product, parentProduct, key]);

    const getProduct = async () => {
        let cidPath = key;
        if (cidPath) {
            let decodeCidPath = decode(cidPath);
            let json = JSON.parse(decodeCidPath);
            let cid = decode(json?.kCid);
            let data = {
                billerMerchantKey: process.env.NEXT_PUBLIC_BILLER_MERCHANTKEY,
                tcsCid: cid,
                dateFrom: "2022-04-21",
                dateTo: "2022-04-21"
            }
            try {
                let res = await POST("/public/biller-merchant-tcs-product/list-bydate", data);
                if (res) {
                    if (res.success) {
                        setParentProduct(res?.result?.result);
                        setProduct(res?.result?.result[0]?.listProduct);
                    }
                }
            } catch (error) {
                console.log(error);
            }
        }
    }

    const goDetailProduct = (item) => {
        let path = Router.router.state.asPath.split("key=")[1];
        let decodePath = decode(path);
        let { adult, kid, product } = JSON.parse(decodePath);
        addLocalStorage({
            product: item
        })
        let result = {
            adult: adult,
            kid: kid,
            product: { ...product }
        }

        Router.push({
            pathname: '/product/buy',
            query: {
                key: encode(JSON.stringify(result))
            }
        });
    }
    return (
        <>
            <MainLayout disableNav={true}>
                <Grid sx={{
                    display: "flex",
                    flexDirection: "row",
                    marginTop: "35px",
                    alignItems: "center",
                    cursor: "pointer"
                }}
                    onClick={() => Router.back()}
                >
                    <ArrowBackIcon fontSize="0.9rem" />
                    <Typography fontSize="0.9rem" marginLeft="5px" fontWeight="500">Back</Typography>
                </Grid>
                <Grid sx={styles.filterTab}>
                    <Grid sx={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "center",
                        width: "100%",
                        height: "50%",
                        borderStyle: "solid",
                        borderRightWidth: "1px",
                        borderLeftWidth: "0px",
                        borderTopWidth: "0px",
                        borderBottomWidth: "0px",
                        borderRightColor: colors.txtCardColor
                    }}
                        onClick={() => handleOpen()}
                    >
                        <SortIcon fontSize="0.8rem" style={{
                            color: colors.txtCardColor
                        }} />
                        <Typography fontSize="0.8rem" marginLeft="5px" fontWeight="500" color={colors.txtCardColor}>Urutkan</Typography>
                    </Grid>
                    <Grid sx={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "center",
                        width: "100%",
                        height: "50%",
                        borderStyle: "solid",
                        borderRightWidth: "1px",
                        borderLeftWidth: "0px",
                        borderTopWidth: "0px",
                        borderBottomWidth: "0px",
                        borderRightColor: colors.txtCardColor
                    }}
                        onClick={() => openFilter(true)}
                    >
                        <FilterAltIcon fontSize="0.8rem" style={{
                            color: colors.txtCardColor
                        }} />
                        <Typography fontSize="0.8rem" marginLeft="5px" fontWeight="500" color="white">Filter</Typography>
                    </Grid>
                    <Grid sx={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "center",
                        width: "100%",
                        height: "100%",
                    }}>
                        <CalendarMonthIcon fontSize="0.8rem" style={{
                            color: colors.txtCardColor
                        }} />
                        <Typography fontSize="0.8rem" marginLeft="5px" fontWeight="500" color={colors.txtCardColor}>Tanggal</Typography>
                    </Grid>
                </Grid>
                <Grid sx={{
                    display: "flex",
                    flexDirection: "column",
                    zIndex: 0
                }}>
                    {
                        product?.map((item, index) => {
                            return (
                                <ListProductCard
                                    key={index}
                                    goDetailProduct={() => goDetailProduct(item)}
                                    data={item}
                                    parentData={parentProduct}
                                />
                            )
                        })
                    }
                </Grid>
            </MainLayout>
            <Sort open={open} handleClose={handleClose} setSort={setSort} />
            <Filter open={isFilter} handleClose={() => openFilter(false)} setSort={setFilter} />
        </>
    );
};

const mapStateToProps = (state) => {
    let { rootReducer } = state;
    return {
        ...rootReducer,
    };
};
export default withRouter(connect(mapStateToProps)(ListProduct));