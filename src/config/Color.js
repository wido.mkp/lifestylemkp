const colors = {
    bgColor: "#FFFFFF",
    btnColor: "white",
    txtColor: "#214A9F",
    btnCategoryColor: "#4581C3",
    bgCompActiveColor: "#2494D2",
    txtCompActiveColor: "white",
    labelCompInactiveColor: "#A7A7A7",
    bgCardColor: "#0075BD",
    txtCardColor: "white",
    hintTextColor: "white",
    bgPassangerModal: "white",
    txtPassangerModal: "#2C2C2C",
    placeholderPassangerColor: "black",
    textDescription: "black"

}
export default colors;