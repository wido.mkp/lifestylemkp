import { Collapse, Grid, Typography } from "@mui/material";
import Divider from '@mui/material/Divider';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import React from "react";
import colors from "src/config/Color";

const SummaryCard = () => {
    const [isCollapse, setCollapse] = React.useState(false);
    return (
        <Grid sx={{
            display: "flex",
            flexDirection: "column",
            width: "100%",
            backgroundColor: colors.bgCardColor,
            height: "auto",
            borderRadius: "13px",
            marginTop: "16px"
        }}>
            <Grid
                sx={{
                    display: "flex",
                    flexDirection: "row",
                    width: "100%",
                    justifyContent: "space-between",
                    height: "auto",
                    borderRadius: "13px",
                }}
            >
                <img
                    src="/assets/images/wisata_a.jpeg"
                    style={{
                        width: "30%",
                        height: "100px",
                        borderTopLeftRadius: "13px",
                        borderBottomLeftRadius: isCollapse ? "0px" : "13px"
                    }} />
                <Grid sx={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    width: "60%",
                    paddingLeft: "16px",
                }}>
                    <p className="titleProduct">
                        Grand Maerakaca
                    </p>
                    {
                        isCollapse ?
                            <p className="labelPassanger">
                                06 Juni 2022
                            </p> :
                            <p className="labelPassanger">
                                4 Tiket Pengunjung
                            </p>
                    }
                </Grid>
                <Grid sx={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "flex-end",
                    alignItems: "center",
                    width: "40%",
                }}>
                    {
                        !isCollapse ?
                            <Grid sx={{
                                display: "flex",
                                flexDirection: "row",
                                justifyContent: "center",
                                alignItems: "center",
                                width: "100%",
                                marginBottom: "13px",
                                cursor: "pointer"
                            }}
                                onClick={() => setCollapse(!isCollapse)}
                            >
                                <Typography fontSize="0.7rem" color={colors.txtCompActiveColor} fontWeight="bold">Detail</Typography>
                                <KeyboardArrowDownIcon sx={{ fontSize: "0.7rem", color: colors.txtCompActiveColor, marginLeft: "5px" }} />
                            </Grid> : null
                    }
                </Grid>
            </Grid>
            <Collapse in={isCollapse}>
                <Grid sx={{
                    margin: "17px",
                    display: "flex",
                    flexDirection: "column",
                }}>
                    <Typography fontSize="0.7rem" color={colors.bgColor}>No. Ref 1909010219022811</Typography>
                    <Typography fontSize="0.7rem" color={colors.bgColor} fontWeight="bold" marginTop="10px">Information</Typography>
                    <Typography fontSize="0.7rem" color={colors.bgColor}>Tanggal 24/06/2022</Typography>
                    <Typography fontSize="0.7rem" color={colors.bgColor}>Tiket masuk Candi Cetha</Typography>
                    <Typography fontSize="0.7rem" color={colors.bgColor}>Visitor 1 John Doe</Typography>
                    <Typography fontSize="0.7rem" color={colors.bgColor}>Visitor 2 John Maxim</Typography>
                    <Typography fontSize="0.7rem" color={colors.bgColor}>Visitor 3 John Statam</Typography>

                    <Typography fontSize="0.7rem" color={colors.bgColor} fontWeight="bold" marginTop="10px">Payment Details</Typography>
                    <Grid sx={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-between",
                        alignItems: "center"
                    }}>
                        <Grid sx={{
                            display: "flex",
                            flexDirection: "column",
                        }}>
                            <Typography fontSize="0.7rem" color={colors.bgColor}>@3 x IDR 5.000</Typography>
                            <Typography fontSize="0.7rem" color={colors.bgColor}>Biaya admin</Typography>
                        </Grid>
                        <Grid sx={{
                            display: "flex",
                            flexDirection: "column",
                            alignItems: "flex-end"
                        }}>
                            <Typography fontSize="0.7rem" color={colors.bgColor}>IDR 15.000</Typography>
                            <Typography fontSize="0.7rem" color={colors.bgColor}>IDR 2.500</Typography>
                        </Grid>
                    </Grid>
                    <Divider sx={{ backgroundColor: colors.bgColor, marginTop: "30px", marginBottom: "30px" }} />
                    <Grid sx={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-between",
                        alignItems: "center"
                    }}>
                        <Typography fontSize="0.7rem" color={colors.bgColor}>Total Pembayaran</Typography>
                        <Typography fontSize="1rem" color={colors.bgColor} fontWeight="bold">IDR 17.500</Typography>
                    </Grid>
                </Grid>
                <Grid sx={{
                    display: "flex",
                    flexDirection: "row",
                    width: "100%",
                    justifyContent: "center",
                    alignItems: "center"
                }}
                    onClick={() => setCollapse(!isCollapse)}
                >
                    <KeyboardArrowUpIcon sx={{ fontSize: "0.8rem", color: colors.txtCompActiveColor, marginBottom: "5px" }} />
                </Grid>
            </Collapse>
            <style jsx>
                {
                    `
                        .titleProduct{
                            display: -webkit-box;
                            text-overflow: ellipsis;
                            -webkit-line-clamp: 2;
                            -webkit-box-orient: vertical;
                            overflow: hidden;
                            font-size: 0.9rem; 
                            color: ${colors.bgColor}; 
                            font-weight: bold;
                            width: 100%
                        }
                        .labelPassanger{
                            display: -webkit-box;
                            text-overflow: ellipsis;
                            -webkit-line-clamp: 1;
                            -webkit-box-orient: vertical;
                            overflow: hidden;
                            font-size: 0.7rem; 
                            color: ${colors.bgColor}; 
                            font-weight: bold;
                            margin-top: -5px;
                        }
                    `
                }
            </style>
        </Grid>
    );
}

export default SummaryCard;