import { Button, Grid, Typography } from '@mui/material';
import Modal from '@mui/material/Modal';
import React from 'react';
import colors from 'src/config/Color';
import DatePicker from "@hassanmojab/react-modern-calendar-datepicker";
import moment from 'moment';

const DatePickerModal = ({ open, handleClose, selectedDay, setSelectedDay, submitForm }) => {
    const renderCustomInput = ({ ref }) => (
        <input
            readOnly
            ref={ref}
            placeholder="Silahkan pilih tanggal"
            value={selectedDay ? `${selectedDay.day}-${selectedDay.month}-${selectedDay.year}` : ''}
            style={{
                textAlign: 'center',
                padding: '0.8rem 0.8rem',
                fontSize: '0.9rem',
                border: '0.1px solid black',
                borderRadius: '10px',
                color: colors.bgCardColor,
                outline: 'none',
                width: "250px"
            }}
        />
    )
    return (
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Grid sx={{
                height: "308px",
                backgroundColor: "white",
                position: 'absolute',
                top: '70%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                width: "350px",
                borderRadius: "5px",
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                color: colors.txtCompActiveColor
            }}>
                <Typography fontWeight="bold" fontSize="1rem" marginBottom="40px" color={colors.bgCompActiveColor}>Tanggal Pemesanan</Typography>
                <DatePicker
                    value={selectedDay}
                    onChange={setSelectedDay}
                    shouldHighlightWeekends
                    renderInput={renderCustomInput}
                />
                <Button style={{
                    backgroundColor: "#228FC7",
                    marginTop: "40px",
                    color: "white"
                }}
                    onClick={() => submitForm(selectedDay ? `${selectedDay.day}-${selectedDay.month}-${selectedDay.year}` : "")}
                >OK</Button>
            </Grid>
        </Modal>
    );
}

export default DatePickerModal;