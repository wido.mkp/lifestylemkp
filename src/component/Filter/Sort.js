import { Button, Grid, Typography } from '@mui/material';
import Modal from '@mui/material/Modal';
import React from 'react';
import colors from 'src/config/Color';
const Sort = ({ open, handleClose, setSort }) => {
    const [isActive, setActive] = React.useState(-1);
    const handleOption = (item) => {
        if (item.id === 1) {
            setActive(item.id);
            setSort(item.value)
        } else if (item.id === 2) {
            setActive(item.id);
            setSort(item.value)
        } else if (item.id === 3) {
            setActive(item.id);
            setSort(item.value)
        }
    }
    return (
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Grid sx={{
                height: "308px",
                backgroundColor: colors.btnColor,
                position: 'absolute',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                width: "350px",
                borderRadius: "5px",
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                color: colors.txtCompActiveColor
            }}>
                <Typography fontSize="1rem" fontWeight="bold" marginBottom="38px" color={colors.bgCompActiveColor}>Urutkan</Typography>
                {
                    [{
                        id: 1,
                        value: "1 Minggu terakhir"
                    },
                    {
                        id: 2,
                        value: "2 Bulan terakhir"
                    },
                    {
                        id: 3,
                        value: "3 Bulan terakhir"
                    }].map((item, index) => {
                        return (
                            <Grid key={index} sx={{
                                width: "70%",
                                display: "flex",
                                flexDirection: "column"
                            }}
                                onClick={() => handleOption(item)}
                            >
                                <Grid sx={{
                                    display: "flex",
                                    flexDirection: "row",
                                    justifyContent: "space-between",
                                    alignItems: "center"
                                }}>
                                    <Typography fontSize="1rem" color={colors.bgCompActiveColor} fontWeight="bold">{item.value}</Typography>
                                    <Grid sx={{
                                        width: "10px",
                                        height: "10px",
                                        borderRadius: "5px",
                                        borderStyle: "solid",
                                        borderWidth: 1,
                                        borderColor: "black",
                                        backgroundColor: isActive === item.id ? "black" : ""
                                    }} />
                                </Grid>
                            </Grid>
                        )
                    })
                }
                <Button sx={{
                    width: "175px",
                    height: "42px",
                    backgroundColor: "#E9E9E9",
                    borderRadius: "5px",
                    marginTop: "50px",
                    color: colors.bgCompActiveColor
                }}
                    onClick={() => handleClose()}
                >Konfirmasi</Button>
            </Grid>
        </Modal>
    );
}

export default Sort;