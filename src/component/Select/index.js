import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import OutlinedInput from '@mui/material/OutlinedInput';
const SelectModule = ({ label, data, onChange, value, style, width, margin = "0px 5px 0px 0px", ...others }) => {
    return (
        <FormControl sx={{ width: width, margin: margin }} size="small">
            <Select
                displayEmpty
                value={value}
                onChange={onChange}
                input={<OutlinedInput />}
                style={style}
                renderValue={(selected) => {
                    if (selected?.length === 0) {
                        return <div style={{ fontSize: "0.8rem" }}>{label}</div>;
                    }

                    return <div style={{ fontSize: "0.8rem" }}>{selected}</div>;
                }}
                inputProps={{ 'aria-label': 'Without label' }}
                {...others}
            >
                {/* <MenuItem disabled value="">
                    <div style={{ fontSize: "0.8rem" }}>{label}</div>
                </MenuItem> */}
                {data?.map((name) => (
                    <MenuItem
                        key={name}
                        value={name}
                        style={{ fontSize: "0.8rem" }}
                    >
                        {name}
                    </MenuItem>
                ))}
            </Select>
        </FormControl>
    );
}

export default SelectModule;