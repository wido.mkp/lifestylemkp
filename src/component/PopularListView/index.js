import { Grid } from '@mui/material';
import { useTheme } from '@mui/styles';
import colors from 'src/config/Color';
import useStyles from "./PopularListView.module";

const PopularListView = ({ data }) => {
    const theme = useTheme();
    const styles = useStyles(theme);
    return (
        <Grid sx={{ ...styles.listProductWrapper, marginTop: "20px" }}>
            {
                data.map((item, index) => {
                    return (
                        item.active ?
                            <Grid key={index} sx={{
                                display: "inline-block",
                                textAlign: "center",
                                padding: "5px 10px 5px 10px",
                                textDecoration: "none",
                                color: colors.txtCompActiveColor,
                                fontSize: "0.8rem",
                                cursor: "pointer",
                                position: "relative",
                                backgroundColor: colors.bgCompActiveColor,
                                borderRadius: "10px",
                                marginRight: "27px",
                            }}>
                                {item.value}
                            </Grid> :
                            <Grid key={index} sx={styles.itemRegion}>
                                {item.value}
                            </Grid>
                    )
                })
            }
        </Grid>
    )
}

export default PopularListView