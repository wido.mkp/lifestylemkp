import colors from "src/config/Color";

const useStyles = theme => ({
    productWrapper: {
        width: "100%",
        textAlign: "left",
        paddingTop: "10px",
        paddingBottom: "10px",
    },
    listProductWrapper: {
        overflowX: "scroll",
        whiteSpace: "nowrap",
        width: "100%",
        marginTop: "24px",
        scrollbarWidth: "none",
        textAlign: "left",
    },
    itemRegion: {
        display: "inline-block",
        textAlign: "center",
        textDecoration: "none",
        color: colors.labelCompInactiveColor,
        fontSize: "13px",
        cursor: "pointer",
        position: "relative",
        marginRight: "27px"
    },
});
export default useStyles;