import MyLocationIcon from '@mui/icons-material/MyLocation';
import { Grid, Typography } from '@mui/material';
import { useTheme } from '@mui/styles';
import useStyles from "./ListView.module";
import ScrollContainer from 'react-indiana-drag-scroll'
import colors from 'src/config/Color';
import ArrowCircleRightIcon from '@mui/icons-material/ArrowCircleRight';
import limitString from "@/utils/limitString";
import Slider from "react-slick";

const ListView = ({ data, onDetailDestination, isEvent = false, onDetailEvent = {} }) => {
    const theme = useTheme();
    const styles = useStyles(theme);
    var settings = {
        dots: false,
        infinite: true,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        centerPadding: "15px",
        centerMode: true,
        autoplay: true,
        autoplaySpeed: 4000,
        cssEase: "linear"
    };
    return (
        <Grid sx={{
            marginTop: "10px",
            marginBottom: "-50px"
        }}>
            <Slider {...settings}>
                {
                    data.map((item, index) => {
                        return (
                            <div
                                key={index}
                                style={{
                                    position: "relative",
                                    height: "230px",
                                    width: "100%",
                                }}>
                                <img src={isEvent ? item?.imageBanner : item?.tcsConfig?.imageBanner} style={{
                                    borderRadius: "10px",
                                    height: "180px",
                                    width: "99%",
                                    objectFit: "cover"
                                }} />
                                <Grid sx={{
                                    position: "relative",
                                    top: "-80px",
                                    left: 0,
                                    right: 0,
                                    marginLeft: "auto",
                                    marginRight: "auto",
                                    width: "90%",
                                    height: "120px",
                                    backgroundColor: "white",
                                    borderRadius: "10px",
                                    boxShadow: "0px 5px 8px -4px rgba(15,15,15,0.75)",
                                    display: "flex",
                                    flexDirection: "column",
                                    alignItems: "flex-start",
                                    padding: "10px",

                                }}>
                                    <Typography sx={{ cursor: "pointer" }} color="black" fontSize="0.6rem" fontWeight="500" textTransform="uppercase">Popular</Typography>
                                    <Typography sx={{ cursor: "pointer", textAlign: "left", marginTop: "5px" }} color="black" fontSize="0.7rem" fontWeight="800">
                                        {
                                            isEvent ? limitString(item?.eventName, 70) : limitString(item?.name, 70)
                                        }
                                    </Typography>
                                    <Grid sx={{
                                        display: "flex",
                                        flexDirection: "row",
                                        justifyContent: "space-between",
                                        flex: 1,
                                        width: "100%"
                                    }}>
                                        <Grid sx={{
                                            display: "flex",
                                            flex: 0.8,
                                        }}>
                                            <Grid sx={{
                                                cursor: "pointer",
                                                textAlign: "left",
                                                marginTop: "5px",
                                                justifyContent: "center",
                                                alignItems: "center",
                                                color: "gray",
                                                fontSize: "0.5rem",
                                                fontWeight: "200"
                                            }} dangerouslySetInnerHTML={isEvent ? { __html: limitString(item?.eventDescription, 82) } : { __html: limitString(item?.tcsConfig?.description, 82) }} />
                                        </Grid>
                                        <Grid
                                            onClick={isEvent ? () => onDetailEvent(item) : () => onDetailDestination(item)}
                                            sx={{
                                                display: "flex",
                                                flex: 0.2,
                                                flexDirection: "row",
                                                justifyContent: "center",
                                                cursor: "pointer",
                                                alignItems: "center"
                                            }}
                                        >
                                            <ArrowCircleRightIcon sx={{ color: colors.bgCompActiveColor }} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </div>
                        );
                    })
                }
            </Slider>
        </Grid>
    )
}

export default ListView