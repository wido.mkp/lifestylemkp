const useStyles = theme => ({
    productWrapper: {
        width: "100%",
        textAlign: "left",
        paddingTop: "2px",
        paddingBottom: "10px",
    },
    listProductWrapper: {
        overflowX: "scroll",
        whiteSpace: "nowrap",
        width: "100%",
        marginTop: "24px",
        scrollbarWidth: "none",
    },
    flexItem: {
        display: "flex",
        flexDirection: "column",
    },
    cardSlide: {
        borderRadius: "10px",
        width: "270px",
        height: "207px"
    },
    productItem: {
        display: "inline-block",
        borderRadius: "15px",
        textAlign: "center",
        textDecoration: "none",
        cursor: "pointer",
        position: "relative",
        marginRight: "10px",
    },
    descCard: {
        position: "absolute",
        width: "208px",
        height: "50px",
        bottom: "10px",
        left: "10px",
        borderRadius: "17px",
        backgroundColor: "rgba(255, 255, 255, 0.73)",
        display: "flex",
        flexDirection: "column"
    }
});
export default useStyles;