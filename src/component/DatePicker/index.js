import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import TextField from '@mui/material/TextField';

const DatePickerModule = ({ placeholder, label, onChange, value, width = "125px", height = "48px" }) => {
    return (
        <>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DateTimePicker
                    inputFormat="tt.mm.yyyy"
                    hideTabs={true}
                    label=""
                    value={value}
                    onChange={onChange}
                    renderInput={(params) => {
                        return (
                            <TextField
                                {...params}
                                InputLabelProps={{
                                    style: {
                                        fontSize: "10px",
                                        height: height
                                    }
                                }}
                                inputProps={{
                                    ...params.inputProps,
                                    placeholder: placeholder,
                                    style: { ...params.style }
                                }}
                                style={{ width: width }}
                                size="small"
                            />
                        );
                    }}
                />
            </LocalizationProvider>
        </>
    );
}

export default DatePickerModule