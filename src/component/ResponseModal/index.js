import Modal from '@mui/material/Modal';
import Image from 'next/image';
import styles from "./ResponseModal.module.css";

const ResponseModal = ({ onClose, open, icon, title, content }) => {
    return (
        <Modal
            open={open}
            onClose={onClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <div className={styles.boxNotif}>
                <Image src={icon} width={108.97} height={109.13} layout="fixed" />
                <div style={{
                    fontSize: "12px",
                    color: "#2B4CAB",
                    fontFamily: "Poppins",
                    fontWeight: "bold",
                    textTransform: "uppercase",
                    marginTop: "27px",
                    marginBottom: "14px"
                }}>{title}</div>
                <div style={{
                    fontSize: "12px",
                    fontFamily: "Poppins",
                    color: "#000000",
                    paddingLeft: "30px",
                    paddingRight: "30px",
                    textAlign: "center"
                }}>
                    {
                        content
                    }
                </div>
            </div>
        </Modal>
    );
}

export default ResponseModal;