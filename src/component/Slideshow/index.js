import React, { useEffect, useRef, useState } from "react";
import Image from 'next/image'
import { Grid, Typography } from "@mui/material";
import useStyles from "./Slideshow.module";
import { useTheme } from "@mui/styles";
import Router from "next/router";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';

const SlideShow = ({ data }) => {
    const [index, setIndex] = useState(0);
    const timeoutRef = useRef(null);
    const theme = useTheme();
    const styles = useStyles(theme);
    const resetTimeout = () => {
        if (timeoutRef.current) {
            clearTimeout(timeoutRef.current);
        }
    }
    useEffect(() => {
        resetTimeout();
        timeoutRef.current = setTimeout(
            () =>
                setIndex((prevIndex) =>
                    prevIndex === data?.length - 1 ? 0 : prevIndex + 1
                ),
            5000
        );

        return () => {
            resetTimeout();
        };
    }, [index]);

    return (
        <Grid sx={styles.slideshow}>
            <Grid
                sx={styles.slideshowSlider}
                style={{ transform: `translate3d(${-index * 100}%, 0, 0)` }}
            >
                {data?.map((value, index) => (
                    <img
                        src={value}
                        key={index}
                        style={{
                            width: "100%",
                            height: "300px"
                        }}
                        loading="lazy"
                    />
                ))}
            </Grid>

            <Grid sx={{
                display: "flex",
                flexDirection: "row",
                position: "absolute",
                top: "34px",
                left: "25px",
                alignItems: "center",
                width: "109px",
                height: "45px",
                justifyContent: "center",
                backgroundColor: "#FFFFFFBA",
                borderRadius: "17px",
                cursor: "pointer"
            }}
                onClick={() => Router.back()}
            >
                <ArrowBackIcon fontSize="0.9rem" />
                <Typography fontSize="0.9rem" marginLeft="5px" fontWeight="500">Back</Typography>
            </Grid>
            {/* <button sx={styles.slideshowButton}></button> */}
            <Grid sx={styles.slideshowDots}>
                {data?.map((value, idx) => {
                    if (index === idx) {
                        return (
                            <Grid
                                key={idx}
                                sx={styles.slideshowDotActive}
                                onClick={() => {
                                    setIndex(idx);
                                }}
                            >
                                <img
                                    src={value}
                                    style={{
                                        width: "100%",
                                        height: "100%",
                                        borderRadius: "5px"
                                    }}
                                    loading="lazy" />
                            </Grid>
                        )
                    } else {
                        return (
                            <Grid
                                key={idx}
                                sx={styles.slideshowDot}
                                onClick={() => {
                                    setIndex(idx);
                                }}
                            >
                                <img
                                    src={value}
                                    style={{
                                        width: "100%",
                                        height: "100%",
                                        borderRadius: "5px"
                                    }}
                                    loading="lazy" />
                            </Grid>
                        )
                    }
                }
                )}
            </Grid>
        </Grid>
    )
}

export default SlideShow;