import { TextField } from '@mui/material';
import React from 'react';

const TextFieldModule = (props) => {
    return (
        <>
            <TextField
                id={props.id ? props.id : "textfield"}
                fullWidth={props.fullWidth ? props.fullWidth : false}
                value={props.value}
                hiddenLabel={props.hiddenLabel ? props.hiddenLabel : false}
                label={props.label}
                variant={props.variant}
                placeholder={props.placeholder ? props.placeholder : ""}
                sx={{
                    width: props.width ? props.width : "100%",
                }}
                InputLabelProps={{
                    style: {
                        fontSize: "0.7rem",
                    }
                }}
                InputProps={props.inputstyle ? {
                    style: props.inputstyle
                } : {
                    style: {
                        fontSize: "0.7rem",
                        width: props.width ? props.width : "100%",
                    }
                }}
                type={props.type ? props.type : "text"}
                size="small"
                {
                ...props
                }
                FormHelperTextProps={{
                    style: {
                        width: props.width ? props.width : "100%",
                    }
                }}
            />
        </>
    );
};

export default TextFieldModule;