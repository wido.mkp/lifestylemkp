import React from "react";
import { Collapse, Grid, Typography, Button } from "@mui/material";
import SelectModule from "../Select";
import TextFieldModule from "../TextField";
import PersonIcon from '@mui/icons-material/Person';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import colors from "src/config/Color";
import limitString from "@/utils/limitString";
import BottomSheet from "../BottomSheet";

const BuyConfirmCard = ({
    index,
    productSelected,
    setFieldValue,
    title,
    id,
    values,
    handleChange,
    product,
    open = false,
    errors,
    touched,
    handleClickDecrement,
    handleClickIncrement
}) => {
    const [isOpen, setOpen] = React.useState(open);
    const [openPanel, setOpenPanel] = React.useState(false);
    let tiketTotal = 0;
    productSelected[index]?.productSelected?.map((item) => {
        tiketTotal += item.value;
    })
    return (
        <Grid>
            <Grid
                sx={{
                    width: "100%",
                    borderRadius: "13px",
                    backgroundColor: colors.bgCardColor,
                    display: "flex",
                    justifyContent: "center",
                    flexDirection: "column",
                    alignItems: "flex-start",
                    padding: "20px",
                    marginTop: "20px",
                    cursor: "pointer"
                }}
                onClick={() => setOpen(!isOpen)}
            >
                <Grid sx={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                }}>
                    <PersonIcon sx={{ color: colors.txtCompActiveColor, fontSize: "1.4rem" }} />
                    <Typography sx={{ color: colors.txtCompActiveColor }} fontSize="0.9rem" marginLeft="10px" fontWeight="bold">{title} {id}</Typography>
                </Grid>
                <Typography sx={{ color: colors.txtCompActiveColor }} fontSize="1.2rem" fontWeight="bold">{limitString(values[`username_${index}`], 25)}</Typography>
                <Grid sx={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    width: "100%",
                    justifyContent: "space-between"
                }}>
                    <Typography sx={{ color: colors.txtCompActiveColor }} fontSize="0.7rem">( {tiketTotal} Tiket )</Typography>
                    <KeyboardArrowDownIcon sx={{ color: "white", fontSize: "1.4rem" }} />
                </Grid>
            </Grid>
            <Collapse sx={{
                width: "100%",
                borderRadius: "13px",
                display: "flex",
                justifyContent: "space-between",
                flexDirection: "row",
                alignItems: "center",
                marginTop: "20px",
                paddingBottom: isOpen ? "40px" : "0px"
            }}
                in={isOpen}>
                <Grid sx={{
                    display: "flex",
                    flexDirection: "column",
                    marginTop: "5px"
                }}>
                    <Typography fontSize="0.8rem" marginBottom="5px" fontWeight="bold">Nama Lengkap</Typography>
                    <Grid sx={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-between",
                        alignItems: "center"
                    }}>
                        <SelectModule
                            style={{
                                backgroundColor: colors.txtCompActiveColor
                            }}
                            label="Tuan"
                            data={["Tuan", "Nyonya"]}
                            width={130}
                            value={values[`title_${index}`]}
                            onChange={e => {
                                setFieldValue(`title_${index}`, e.target.value);
                            }}
                            name={`title_${index}`}
                        />
                        <TextFieldModule
                            hiddenLabel={true}
                            placeholder="Nama Sesuai Identitas"
                            fullWidth={true} inputstyle={{
                                backgroundColor: "white",
                                fontSize: "0.8rem",
                                borderRadius: "6px",
                                height: "40px",
                            }}
                            id={`username_${index}`}
                            name={`username_${index}`}
                            value={values[`username_${index}`]}
                            onChange={e => {
                                setFieldValue(`username_${index}`, e.target.value);
                            }}
                            error={touched[`username_${index}`] && Boolean(errors[`username_${index}`])}
                            helperText={touched[`username_${index}`] && errors[`username_${index}`]}
                        />
                    </Grid>
                </Grid>
                <Grid sx={{
                    display: "flex",
                    flexDirection: "column",
                    marginTop: "16px"
                }}>
                    <Typography fontSize="0.8rem" marginBottom="5px" fontWeight="bold">Identitas</Typography>
                    <Grid sx={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-between",
                        alignItems: "center"
                    }}>
                        <SelectModule
                            style={{
                                backgroundColor: colors.txtCompActiveColor
                            }}
                            label="KTP"
                            data={["KTP", "SIM", "Passport"]}
                            width={130}
                            value={values[`identity_${index}`]}
                            onChange={e => {
                                setFieldValue(`identity_${index}`, e.target.value);
                            }}
                            name={`identity_${index}`}
                        />
                        <TextFieldModule
                            hiddenLabel={true}
                            placeholder="32xxxxxxxxxxx"
                            fullWidth={true}
                            inputstyle={{
                                backgroundColor: colors.txtCompActiveColor,
                                fontSize: "0.8rem",
                                borderRadius: "6px",
                                height: "40px",
                            }}
                            id={`document_${index}`}
                            name={`document_${index}`}
                            value={values[`document_${index}`]}
                            onChange={e => {
                                setFieldValue(`document_${index}`, e.target.value);
                            }}
                            error={touched[`document_${index}`] && Boolean(errors[`document_${index}`])}
                            helperText={touched[`document_${index}`] && errors[`document_${index}`]}
                        />
                    </Grid>
                </Grid>
                <Grid sx={{
                    display: "flex",
                    flexDirection: "column",
                    marginTop: "16px"
                }}>
                    <Typography fontSize="0.8rem" marginBottom="5px" fontWeight="bold">Handphone</Typography>
                    <TextFieldModule
                        hiddenLabel={true}
                        type="number"
                        placeholder="No. Handphone"
                        fullWidth={true} inputstyle={{
                            backgroundColor: colors.txtCompActiveColor,
                            height: "40px",
                            fontSize: "0.8rem",
                            borderRadius: "6px"
                        }}
                        id={`telephone_${index}`}
                        name={`telephone_${index}`}
                        value={values[`telephone_${index}`]}
                        onChange={e => {
                            setFieldValue(`telephone_${index}`, e.target.value);
                        }}
                        error={touched[`telephone_${index}`] && Boolean(errors[`telephone_${index}`])}
                        helperText={touched[`telephone_${index}`] && errors[`telephone_${index}`]}
                    />
                </Grid>
                <Grid sx={{
                    display: "flex",
                    flexDirection: "column",
                    marginTop: "16px"
                }}>
                    <Typography fontSize="0.8rem" marginBottom="5px" fontWeight="bold">Gender</Typography>
                    <SelectModule
                            style={{
                                backgroundColor: colors.txtCompActiveColor
                            }}
                            label="Laki-laki"
                            data={["Male", "Female"]}
                            width="100%"
                            value={values[`gender_${index}`]}
                            onChange={e => {
                                setFieldValue(`gender_${index}`, e.target.value);
                            }}
                            name={`gender_${index}`}
                        />
                </Grid>
                <Grid sx={{
                    display: "flex",
                    flexDirection: "column",
                    marginTop: "16px"
                }}>
                    <Typography fontSize="0.8rem" marginBottom="5px" fontWeight="bold">Email</Typography>
                    <TextFieldModule
                        hiddenLabel={true}
                        placeholder="Masukkan Email"
                        fullWidth={true} inputstyle={{
                            backgroundColor: colors.txtCompActiveColor,
                            height: "40px",
                            fontSize: "0.8rem",
                            borderRadius: "6px"
                        }}
                        id={`email_${index}`}
                        name={`email_${index}`}
                        value={values[`email_${index}`]}
                        onChange={e => {
                            setFieldValue(`email_${index}`, e.target.value);
                        }}
                        error={touched[`email_${index}`] && Boolean(errors[`email_${index}`])}
                        helperText={touched[`email_${index}`] && errors[`email_${index}`]}
                    />
                </Grid>
                <Grid sx={{
                    display: "flex",
                    flexDirection: "column",
                    marginTop: "16px"
                }}>
                    <Typography fontSize="0.8rem" marginBottom="5px" fontWeight="bold">Pilih Produk</Typography>
                    <Button
                        type="button"
                        sx={{
                            backgroundColor: colors.bgCardColor,
                            fontSize: "0.9rem",
                            textTransform: "capitalize",
                            borderRadius: "6px",
                            width: "100%",
                            height: "40px",
                            color: colors.txtCompActiveColor,
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "space-between",
                            alignItems: "center",
                            paddingLeft: "15px",
                            "&:hover": {
                                color: 'gray',
                                backgroundColor: 'lightblue'
                            }
                        }}
                        onClick={() => setOpenPanel(true)}
                    >
                        <Typography fontSize="0.8rem">{tiketTotal} Produk</Typography>
                        <KeyboardArrowDownIcon sx={{ color: "white", fontSize: "1.4rem" }} />
                    </Button>
                </Grid>
            </Collapse>
            <BottomSheet isOpen={openPanel} onChange={setOpenPanel}>
                <Grid sx={{
                    height: "80%",
                    display: "flex",
                    flexDirection: "column",
                    paddingLeft: "25px",
                    paddingRight: "25px",
                    paddingTop: "25px"
                }}>
                    {
                        product?.map((item, index_item) => {
                            return (
                                <Grid
                                    key={index_item}
                                    sx={{
                                        width: "100%",
                                        height: "100px",
                                        borderRadius: "10px",
                                        backgroundImage: `url(https://salsawisata.b-cdn.net/wp-content/uploads/2022/04/tempat-wisata-di-indonesia-yang-terkenal.jpg)`,
                                        backgroundRepeat: 'no-repeat',
                                        backgroundSize: 'cover ',
                                        display: "flex",
                                        flexDirection: "row",
                                        justifyContent: "space-between",
                                        paddingLeft: "10px",
                                        paddingRight: "10px",
                                        marginBottom: "10px"
                                    }}>
                                    <Grid sx={{
                                        display: "flex",
                                        flexDirection: "column",
                                        justifyContent: "center",
                                        alignItems: "flex-start",
                                        flex: 0.65
                                    }}>
                                        <Typography sx={{ color: colors.txtCompActiveColor }} fontSize="0.9rem" fontWeight="bold">{item?.productName} </Typography>
                                        <Typography sx={{ color: colors.txtCompActiveColor }} fontSize="0.9rem">Rp. {item?.b2cPrice} </Typography>
                                    </Grid>
                                    <Grid sx={{
                                        display: "flex",
                                        flexDirection: "row",
                                        justifyContent: "space-between",
                                        alignItems: "center",
                                        flex: 0.35
                                    }}>
                                        <Grid sx={{
                                            width: "2.5rem",
                                            height: "2.5rem",
                                            borderStyle: "solid",
                                            borderWidth: 1,
                                            borderColor: "white",
                                            borderRadius: "15px",
                                            display: "flex",
                                            flexDirection: "row",
                                            justifyContent: "center",
                                            alignItems: "center"
                                        }}
                                            onClick={() => handleClickDecrement(index, index_item)}
                                        >
                                            <Typography sx={{ color: "white" }} fontSize="2rem" fontWeight="bold">-</Typography>
                                        </Grid>
                                        <Typography sx={{ color: "white" }} fontSize="1.5rem" fontWeight="bold">{productSelected[index]?.productSelected[index_item]?.value || 0}</Typography>
                                        <Grid sx={{
                                            width: "2.5rem",
                                            height: "2.5rem",
                                            borderStyle: "solid",
                                            borderWidth: 1,
                                            borderColor: "white",
                                            borderRadius: "15px",
                                            display: "flex",
                                            flexDirection: "row",
                                            justifyContent: "center",
                                            alignItems: "center"
                                        }}
                                            onClick={() => handleClickIncrement(index, index_item)}
                                        >
                                            <Typography sx={{ color: "white" }} fontSize="2rem" fontWeight="bold">+</Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            );
                        })
                    }
                </Grid>
            </BottomSheet>
        </Grid>
    );
}

export default BuyConfirmCard