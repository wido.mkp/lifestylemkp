import limitString from "@/utils/limitString";
import { Button, Grid, Typography } from "@mui/material";
import colors from "src/config/Color";

const ListProductCard = ({ goDetailProduct, data, parentData }) => {
    let tcsName = data?.productName.toLowerCase();
    let city = parentData[0]?.cityName.toLowerCase();
    let province = parentData[0]?.provinceName.toLowerCase();
    return (
        <Grid sx={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            width: "100%",
            height: "91px",
            backgroundColor: colors.bgCardColor,
            borderRadius: "13px",
            marginTop: "30px",
            padding: "13px"
        }}>
            <Grid sx={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "flex-start",
                alignItems: "flex-start",
                flex: 0.6
            }}>
                <Typography
                    fontSize="0.9rem"
                    fontWeight="bold"
                    color={colors.txtCardColor}
                    sx={{
                        textTransform: "uppercase",
                        textAlign: "left",
                    }}
                >
                    {limitString(tcsName, 15)}
                </Typography>
                <Typography
                    fontSize="0.7rem"
                    marginTop="10px"
                    sx={{
                        textTransform: "capitalize",
                        overflow: "hidden"
                    }}
                    fontWeight="500"
                    color={colors.hintTextColor}
                    textAlign="left"
                >
                    {limitString(`${city}, ${province}`, 45)}
                </Typography>
            </Grid>
            <Grid sx={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "flex-start",
                alignItems: "flex-end",
                flex: 0.4
            }}>
                <Typography fontSize="0.9rem" fontWeight="bold" color={colors.txtCardColor}>Rp {data?.b2cPrice}</Typography>
                <Button
                    onClick={() => {
                        goDetailProduct();
                    }}
                    sx={{
                        marginTop: "10px",
                        width: "79px",
                        height: "27px",
                        backgroundColor: colors.txtCardColor,
                        borderRadius: "28px",
                        fontSize: "0.7rem",
                        textTransform: "capitalize",
                        color: colors.bgCardColor,
                    }}>More Info</Button>
            </Grid>
        </Grid>
    );
}

export default ListProductCard;