const useStyles = theme => ({
    productWrapper: {
        width: "100%",
        textAlign: "left",
        paddingTop: "10px",
        paddingBottom: "10px",
    },
    listProductWrapper: {
        overflowX: "scroll",
        whiteSpace: "nowrap",
        width: "100%",
        marginTop: "24px",
        scrollbarWidth: "none",
    },
});
export default useStyles;