import { Grid, Typography } from '@mui/material';
import { useTheme } from '@mui/styles';
import Image from 'next/image';
import colors from 'src/config/Color';
import useStyles from "./CategoryListView.module";

const CategoryListView = ({ data, onClickProduct }) => {
    const theme = useTheme();
    const styles = useStyles(theme);
    return (
        <Grid>
            <Grid sx={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center"
            }}>
                <Typography sx={{ cursor: "pointer" }} mt={2} fontSize="0.8rem" fontWeight="500" textTransform="capitalize">Categories</Typography>
                <Typography sx={{ cursor: "pointer" }} mt={2} fontSize="0.8rem" fontWeight="500" textTransform="capitalize">See all</Typography>
            </Grid>
            <Grid sx={styles.listProductWrapper}>
                {
                    data.map((value, index) => {
                        return (
                            <Grid
                                key={index}
                                sx={{
                                    display: "inline-block",
                                    cursor: "pointer"
                                }}
                                onClick={()=>onClickProduct(value)}
                            >
                                <Grid key={index} sx={{
                                    backgroundColor: colors.btnCategoryColor,
                                    borderRadius: "13px",
                                    marginRight: "22px",
                                    width: "59px",
                                    height: "59px",
                                }}>
                                    <Grid sx={{
                                        display: "flex",
                                        flexDirection: "row",
                                        width: "100%",
                                        height: "100%",
                                        justifyContent: "center",
                                        alignItems: "center"
                                    }}>
                                        <Image src={value.source} width={30} height={23} />
                                    </Grid>
                                </Grid>
                                <Grid sx={{
                                    width: "59px",
                                    display: "flex",
                                    flexDirection: "row",
                                    alignItems: "center",
                                    justifyContent: "center"
                                }}>
                                    <Typography whiteSpace="initial" textAlign="center" mt={2} fontSize="0.7rem" fontWeight="500" textTransform="capitalize">
                                        {value.name}
                                    </Typography>
                                </Grid>
                            </Grid>
                        )
                    })
                }
            </Grid>
        </Grid>
    )
}

export default CategoryListView