import { Grid } from "@mui/material";
import { useTheme } from "@mui/styles";
import useStyles from "./LoadingScreen.module";

const LoadingScreen = () => {
    const theme = useTheme();
    const styles = useStyles(theme);
    return (
        <Grid sx={styles.container}>
            <img src="/images/loading.gif" alt="loading..." style={{
                width: "80px",
                height: "80px"
            }} />
            {/* <CircularProgress color="info" size={100}/> */}
        </Grid>
    );
}

export default LoadingScreen;