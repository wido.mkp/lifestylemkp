import React from "react";
import Card from '@mui/material/Card';
import Image from "next/image";
import useStyles from "./HistoryCard.module.js";
import Collapse from '@mui/material/Collapse';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import { useTheme } from "@mui/styles";
import { Grid } from "@mui/material";

const HistoryCardItem = ({
    vehicleName,
    date,
    isShowDate = true,
    children,
    icon = "car",
    isCollapse = false,
    expandComponent = {}
}) => {
    const [checked, setChecked] = React.useState(false);
    const theme = useTheme();
    const styles = useStyles(theme);
    const handleChange = () => {
        setChecked((prev) => !prev);
    };
    return (
        <Card sx={{
            width: "100%",
            height: "auto",
            borderRadius: "5px",
            display: "flex",
            flexDirection: "column",
            marginBottom: "27px"
        }}>
            <Card sx={{
                width: "100%",
                height: "31px",
                borderTopLeftRadius: "5px",
                borderTopRightRadius: "5px",
                borderBottomRightRadius: "0px",
                borderBottomLeftRadius: "0px",
                backgroundColor: "#2B4CAB",
                display: "flex",
                flexDirection: "row",
                paddingLeft: "15px",
                paddingRight: "15px",
                justifyContent: "space-between"
            }}>
                <Grid sx={styles.vehicleLabelIcon}>
                    <Image
                        src={icon === "car" ? "/images/car.png" : "/images/motorcycle.png"}
                        width={33}
                        height={12.33}
                        layout="fixed"
                        objectFit="contain"
                    />
                    <Grid style={{
                        fontSize: "10px",
                        fontFamily: "Poppins",
                        color: "#FFFFFF",
                        marginLeft: "10px",
                        textTransform: "uppercase"
                    }}>| {vehicleName}</Grid>
                </Grid>
                <Grid sx={styles.vehicleLabelIcon}>
                    <Grid style={{
                        fontSize: "10px",
                        fontFamily: "Poppins",
                        color: "#FFFFFF",
                        marginLeft: "10px",
                        textTransform: "uppercase"
                    }}>{isShowDate ? date : null}</Grid>
                </Grid>
            </Card>
            <Grid sx={styles.cardContent}>
                {
                    children
                }
            </Grid>
            {
                isCollapse ? checked ? null :
                    <Grid style={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "center",
                        alignItems: "center",
                        width: "100%",
                        height: "20px",
                    }}
                        onClick={handleChange}
                    >
                        <KeyboardArrowDownIcon style={{ width: "30px", height: "20px" }} />
                    </Grid> : null
            }
            {
                isCollapse ? <>
                    <Collapse in={checked} sx={styles.cardExpand}>
                        {checked ? expandComponent : null}
                    </Collapse>
                </> : null
            }
            {
                isCollapse ? checked ? <Grid style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                    width: "100%",
                    height: "20px",
                }}
                    onClick={handleChange}
                >
                    <KeyboardArrowUpIcon style={{ width: "30px", height: "20px" }} />
                </Grid> : null : null
            }
        </Card>
    );
}

export default HistoryCardItem;