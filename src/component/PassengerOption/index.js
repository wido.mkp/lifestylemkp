import { Button, Grid, Typography } from '@mui/material';
import Modal from '@mui/material/Modal';
import React from 'react';
import CloseIcon from '@mui/icons-material/Close';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import colors from 'src/config/Color';

const PassangerOption = ({ open, data, handleClose, setDataPassanger = {}, isEvent = false, handleConfirmToCheckout = () => { } }) => {
    const [adult, setAdult] = React.useState(0);
    const [kid, setKid] = React.useState(0);
    const handleAdultSum = () => {
        setAdult(adult + 1);
    }
    const handleAdultMinus = () => {
        if (adult > 0) {
            setAdult(adult - 1);
        }
    }
    const handleKidSum = () => {
        setKid(kid + 1);
    }
    const handleKidMinus = () => {
        if (kid > 0) {
            setKid(kid - 1);
        }
    }
    const handleConfirm = () => {
        setDataPassanger({
            adult: adult,
            kid: kid
        });
        handleClose();
    }

    const handleConfirmEvent = () => {
        setDataPassanger({
            adult: adult,
            kid: kid
        });
        let result = {
            adult: adult,
            parent: data
        }
        handleConfirmToCheckout(result);
    }

    return (
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Grid sx={{
                height: "400px",
                backgroundColor: colors.bgPassangerModal,
                position: 'absolute',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                width: "350px",
                borderRadius: "5px",
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'flex-start',
                alignItems: "center",
                color: colors.txtPassangerModal,
                padding: "20px"
            }}>
                <Grid sx={{
                    width: "100%",
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                }}>
                    <Typography fontSize="1rem" fontWeight="bold">Pilih Penumpang</Typography>
                    <CloseIcon onClick={() => handleClose()} sx={{ fontSize: "1rem" }} />
                </Grid>
                <Grid sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: "center",
                    marginTop: "70px"
                }}>
                    <Typography fontSize="0.9rem" fontWeight="bold">Pengunjung</Typography>
                    <Grid sx={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-between",
                        alignItems: "center",
                        width: "279px",
                        height: "40px",
                        backgroundColor: "#FFFFFF",
                        borderRadius: "6px",
                        marginTop: "10px",
                        boxShadow: `0.1px 0.1px 1px ${colors.txtPassangerModal}`
                    }}>
                        <Grid sx={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "center",
                            alignItems: "center",
                            width: "20%",
                            height: "100%",
                            backgroundColor: "#1183B8",
                            borderTopLeftRadius: "6px",
                            borderBottomLeftRadius: "6px"
                        }}
                            onClick={() => handleAdultMinus()}
                        >
                            <KeyboardArrowDownIcon sx={{ fontSize: "1.5rem", color: colors.bgPassangerModal }} />
                        </Grid>
                        <Grid sx={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "center",
                            alignItems: "center",
                            width: "60%",
                            height: "100%",
                        }}>
                            <Typography fontSize="0.9rem" color={colors.placeholderPassangerColor}>{adult} Orang</Typography>
                        </Grid>
                        <Grid sx={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "center",
                            alignItems: "center",
                            width: "20%",
                            height: "100%",
                            backgroundColor: "#0E6D99",
                            borderTopRightRadius: "6px",
                            borderBottomRightRadius: "6px"
                        }}
                            onClick={() => handleAdultSum()}
                        >
                            <KeyboardArrowUpIcon sx={{ fontSize: "1.5rem", color: colors.bgPassangerModal }} />
                        </Grid>
                    </Grid>
                    {/* <Typography marginTop="10px" fontSize="0.9rem" fontWeight="bold">Kid (Dibawah 5 tahun) </Typography>
                    <Grid sx={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-between",
                        alignItems: "center",
                        width: "279px",
                        height: "40px",
                        backgroundColor: "#FFFFFF",
                        borderRadius: "6px",
                        marginTop: "10px",
                        boxShadow: `0.1px 0.1px 1px ${colors.txtPassangerModal}`
                    }}>
                        <Grid sx={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "center",
                            alignItems: "center",
                            width: "20%",
                            height: "100%",
                            backgroundColor: "#A3A2A1",
                            borderTopLeftRadius: "6px",
                            borderBottomLeftRadius: "6px"
                        }}
                            onClick={() => handleKidMinus()}
                        >
                            <KeyboardArrowDownIcon sx={{ fontSize: "1.5rem", color: colors.bgPassangerModal }} />
                        </Grid>
                        <Grid sx={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "center",
                            alignItems: "center",
                            width: "60%",
                            height: "100%",
                        }}>
                            <Typography fontSize="0.9rem" color={colors.placeholderPassangerColor}>{kid} Orang</Typography>
                        </Grid>
                        <Grid sx={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "center",
                            alignItems: "center",
                            width: "20%",
                            height: "100%",
                            backgroundColor: "#040404",
                            borderTopRightRadius: "6px",
                            borderBottomRightRadius: "6px"
                        }}
                            onClick={() => handleKidSum()}
                        >
                            <KeyboardArrowUpIcon sx={{ fontSize: "1.5rem", color: colors.bgPassangerModal }} />
                        </Grid>
                    </Grid> */}
                </Grid>
                <Button sx={{
                    width: "175px",
                    height: "42px",
                    backgroundColor: "#E9E9E9",
                    borderRadius: "5px",
                    marginTop: "50px",
                    color: "black",
                    "&:hover": {
                        color: 'gray',
                        backgroundColor: '#f0f2f0'
                    }
                }}
                    onClick={isEvent ? () => handleConfirmEvent() : () => handleConfirm()}
                >{isEvent ? "Lanjut" : "Konfirmasi"}</Button>
            </Grid>
        </Modal>
    );
}

export default PassangerOption;