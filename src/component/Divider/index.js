const Divider = (props) => {
    return (
        <>
            <div className="divider" />
            <style jsx>
                {
                    `
                        .divider{
                            height: ${props.height};
                            width: ${"100%"||props.width};
                            background-color: #E5E5E5;
                        }
                    `
                }
            </style>
        </>
    );
};
export default Divider;