import {Base64} from 'js-base64';
export const POST = async (pathname, data, header = {}) => {
    let url = process.env.NEXT_PUBLIC_API_URL + pathname;
    let authString = `${process.env.NEXT_PUBLIC_USERNAME}:${process.env.NEXT_PUBLIC_PASSWORD}`
    const response = await fetch(url, {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': "Basic " + Base64.btoa(authString),
            ...header
        },
        body: JSON.stringify(data)
    });
    return response.json();
}

export const GET = async (pathname, header = {}) => {
    let url = process.env.NEXT_PUBLIC_API_URL + pathname;
    let authString = `${process.env.NEXT_PUBLIC_USERNAME}:${process.env.NEXT_PUBLIC_PASSWORD}`
    const response = await fetch(url, {
        method: 'GET',
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': "Basic " + Buffer.from(authString, 'base64'),
            ...header
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer'
    });
    return response.json();
}

