/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ['www.gotravelly.com','http://119.2.53.151'],
  },
}

module.exports = nextConfig
